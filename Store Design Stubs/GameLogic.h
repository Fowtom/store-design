//
// Created by Tommy on 12/2/2016.
//

#ifndef STORE_DESIGN_STUBS_GAMELOGIC_H
#define STORE_DESIGN_STUBS_GAMELOGIC_H
#include <iostream>
#include "Game.h"
using namespace std;

class GameLogic : public Game
{
    private:
        string gameUserInput;

    public:

        /**
         * This is the main driver method for the arena. It starts off by asking if the
         * player would like to view their stats, equip an item, unequip an item, go to
         * the arena, go to the store, or exit. It will also print out the key letter
         * associated with each option. Store = "z", Arena = "x", Equip = "c", Unequip
         * = "v", View stats = "b", Exit = "n". After prompting the user for input, the
         * user will be redirected depending on their input. Their input determines which
         * begin[task]Logic() method will be called.
         */
        void promptGameInput();

        /**
         * This method contains the logic for the store. It will follow through the logic
         * until the user decides to leave the store. When that is done, the user's file
         * will be saved and promptGameInput() will be called.
         */
        void beginStoreLogic();

        /**
         * This method contains the logic for the arena. It will follow through the logic
         * until the user leaves the arena. If they leave the arena, it will save and call
         * promptGameInput(). If they beat the arena, the game will end and it will exit
         * the code.
         */
        void beginArenaLogic();

        /**
         * This method simply saves the file and exits the code.
         */
        void beginExitLogic();

        /**
         * This method is used for equipping an item. It checks to see if the bag is empty and
         * if there are any weapons / armor in the bag. It then prints out the player's bag
         * and prompts the user to input an item. It checks to see if that item is a weapon
         * or armor, and if it is, it will pop the item out of the bag and push it into
         * the player's equipped item vector, adding the item stats to the player's stats.
         * After this, it will print feedback and call promptGameInput().
         */
        void beginEquipLogic();

        /**
         * This method is used for unequipping an item. It check to see if the player has any
         * equipped items. It then prints out all the player's equipped items
         * and prompts the user to input an item. It will pop the item out of the equippedItem
         * vector and push it into the player's bag, subtracting the item stats from the player's stats.
         * After this, it will print feedback and call promptGameInput().
         */
        void beginUnequipLogic();

        /**
         * This method is used for viewing the player's stats. It starts off by checking if the
         * player has any items equipped. It will then print out feedback with those items and the
         * player's stats, and then call promptGameInput().
         */
        void beginStatsLogic();

        /**
         * This method is used for welcoming the user to the game, as well as giving the user the option
         * to load a previous save file or start a new game.
         */
        void beginstartLogic();
};


#endif //STORE_DESIGN_STUBS_GAMELOGIC_H
