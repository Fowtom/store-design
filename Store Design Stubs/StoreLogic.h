//
// Created by Tommy on 11/5/2016.
//

#ifndef STORE_DESIGN_STUBS_STORELOGIC_H
#define STORE_DESIGN_STUBS_STORELOGIC_H

#include "Store.h"

class StoreLogic : public Store
{
    private:
        string userInput;

    public:

        /**
         * Prints out instructions for the what the user can input.
         * Contains the logic of the decision making process by validating
         * the input and calling necessary functions for each of the options
         * the user has available.
         */
        void promptStoreInput();

        /**
         * beginaddtoCartLogic() gets called when the user inputs the value associated
         * with the "Add to Cart" selection. It starts off by checking to see if the
         * inventory is empty, because if it is, then it will return an error. After that,
         * it prompts the user to input the name of the item he/she would like to buy within
         * a while loop that validates the input. After the input has been declared valid, the
         * selected item will be taken out of the inventory and into the user's cart, and
         * promptInput() will be called.
         */
        void beginaddtoCartLogic();

        /**
         * beginsellLogic() gets called when the user inputs the value associated
         * with the "Sell" selection. It starts off by checking to see if the user's bag
         * is empty, because if it is, they won't be able to sell anything. After this, the
         * user will be prompted to select an item from his/her bag, and this input is validated
         * within a while loop. Then the item is taken from the bag and it will print out the user's
         * balance after the cost of the item has been added to the player's balance. It will then
         * call promptInput() and bring the user back to the store menu.
         */
        void beginsellLogic();

        /**
         * beginviewCartLogic() gets called when the user inputs the value associated
         * with the "View Cart" selection. It starts off by checking the size of the cart
         * and the bag to check if whether or not either one is empty. If either one is
         * empty, the user will receive feedback accordingly. If either one is not empty,
         * it will print out the contents of the cart and/or bag.
         */
        void beginviewCartandBagLogic();

        /**
         * beginremovefromCartLogic() gets called when the user inputs the value associated
         * with the "Remove from Cart" selection. It starts off by checking the size of the cart
         * to see if it is empty, because if it is, it will give an error and return to the store menu.
         * If the cart is not empty, its contents will be printed out. It will then prompt the user for
         * input. If the input matches one of the items in the cart, the item will be moved from the cart
         * to the inventory.
         */
        void beginremovefromCartLogic();

        /**
         * begincheckoutLogic gets called when the user inputs the value associated
         * with the "Checkout" selection. It starts off by checking the size of the cart to see if it is
         * empty, because if it is, it will give an error and return to the store menu. If the user's cart
         * is not empty, it will print out the user's balance and the cart balance. It will prompt the user
         * if he/she would like to remove any items from his/her cart before checking out. If "yes", it will
         * call beginremovefromCartLogic, and if "no" it will proceed. It will then inform the user of what items
         * he/she will be buying and what the total amount will cost and how much he/she will have left. After one
         * final confirmation for the checkout, the cart balance will be subtracted from the user's balance, unless
         * the user does not have enough gold, and it will return to promptInput().
         */
        void begincheckoutLogic();

        /**
         * beginleaveLogic() gets called when the user inputs the value associated
         * with the "Leave" selection. It starts off by prompting the user if he/she would like to leave
         * the store. If "yes", it will print out "Thank you for using the store!" and it will exit the code.
         * If anything else is typed, promptInput() will be called instead.
         */
        void beginleaveLogic();
};


#endif //STORE_DESIGN_STUBS_STORELOGIC_H
