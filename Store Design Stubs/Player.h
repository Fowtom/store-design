//
// Created by Tommy on 11/3/2016.
//

#ifndef STORE_DESIGN_STUBS_PLAYER_H
#define STORE_DESIGN_STUBS_PLAYER_H

#include "BaseInventory.h"

class Player
{
    private:
        static vector <BaseSalableItem*> bag;
        static vector <BaseSalableItem*> equippedItems;
        static string name;
        static int playerBalance;
        static int health;
        static int playerAttackValue;
        static int playerDefendValue;
        static Player* inst;
        Player();

    public:

        /**
         * Destructor for Player, deletes
         * all the contents of the bag
         */
        ~Player();

        /**
         * Getter method for returning the user's bag
         * @return the bag vector
         */
        vector <BaseSalableItem*> get_bag();

        /**
         * Getter method for returning the user's balance
         * @return playerBalance member variable
         */
        int get_playerBalance();

        /**
         * Prints out contents of bag by iterating through the
         * size of the bag and printing out each item.
         */
        void printBag();

        /**
         * Adds gold to balance after selling item(s).
         * The amount of gold will depend on the value of the parameter.
         * @param gold The currency that is being added to the user's balance
         */
        void addtoplayerBalance(int gold);

        /**
         * Removes gold from balance after buying item(s).
         * The amount of gold will depend on the value of the parameter.
         * @param gold The currency that is being removed from user's balance
         */
        void removefromplayerBalance(int gold);

        /**
         * Adds an item to the user's bag when the user checks out
         * @param item Pointer item of type BaseSalableItem (what is
         * being added to the bag).
         */
        void addtoBag(BaseSalableItem *item);

        /**
         * Removes an item from the bag by swapping the position of the item
         * and popping it out.
         * @param item Of type integer for iteration purposes. It swaps the
         * position of the item to the back of the vector so it can be popped out.
         */
        void removefromBag(int item);

        /**
         * Checks the size of the bag
         * @return True (1) if the bag has at least 1 item in it.
         * Otherwise, return false (0).
         */
        bool checkBagSize();

        /**
         * Creates static pointer instance of player. This is used for setting the
         * player instance originally in the store equal to the player instances in
         * the game and arena.
         * @return player instance
         */
        static Player* instance();

        /**
         * This method prints out all items in the equippedItems vector.
         */
        void printequippedItems();

        /**
         * Getter method for equippedItems vector
         * @return equippedItems
         */
        vector <BaseSalableItem*> get_equippedItems();

        /**
         * Push back method that adds pointer instances of BaseSalableItem
         * to the equippedItems vector
         * @param item or type weapon or armor
         */
        void addtoequippedItems(BaseSalableItem *item);

        /**
         * Setter method for adding stat to player's attack value
         * @param stat from weapon item
         */
        void addplayerattackValue(int stat);

        /**
         * Setter method for adding stat to player's defend value
         * @param stat from armor item
         */
        void addplayerdefendValue(int stat);

        /**
         * Popback method that removes items from equippedItems
         * @param item of type weapon or armor
         */
        void removeequippedItems(int item);

        /**
         * Setter method for subtracting player's defend value
         * @param stat from armor item
         */
        void removeplayerdefendValue(int stat);

        /**
         * Setter method for subtracting player's attack value
         * @param stat from weapon item
         */
        void removeplayerattackValue(int stat);

        /**
         * Method that prints out the player's name, health, attackValue,
         * and defendValue.
         */
        void printplayerStats();

        /**
         * Setter method that adds to the player's health the stat value of
         * the item that is being consumed
         * @param stat from Health item
         */
        void addplayerhealth(int stat);

        /**
         * Setter method that subtracts from the player's health the stat value
         * of the enemy's attack
         * @param stat refers to enemyAttackValue
         */
        void removeplayerhealth(int stat);

        /**
         * Getter method for the player's health
         * @return health
         */
        int get_playerhealth();

        /**
         * Getter method for player's attackValue
         * @return playerAttackValue
         */
        int get_playerattackValue();

        /**
         * Getter method for player's defendValue
         * @return playerDefendValue
         */
        int get_playerdefendValue();

        /**
         * Sets the player's health equal to the stat in the parameter.
         * This is used for restoring the player to full health
         * @param stat generally the player's maximum health
         * @return health
         */
        int setplayerhealth(int stat);

        /**
         * Getter method for the player's name
         * @return name
         */
        string get_name();

        /**
         * Setter method for player's name. Used for loading
         * the player's file.
         * @param name Player's name
         * @return name = name
         */
        void setplayername(string playername);

        /**
         * Setter method for player's attack value. Used for
         * loading the player's file
         * @param stat playerAttackValue
         * @return playerAttackValue = stat
         */
        void setplayerattackValue(int stat);

        /**
         * Setter method for player's defend value. Used for
         * loading the player's file.
         * @param stat playerDefendValue
         * @return playerDefendValue = stat
         */
        void setplayerdefendValue(int stat);

        /**
         * Setter method for player's balance. Used for
         * loading the player's file.
         * @param gold playerBalance
         * @return playerBalance = gold
         */
        void setplayerBalance(int gold);

        void addtobag(BaseSalableItem *item);
};


#endif //STORE_DESIGN_STUBS_PLAYER_H
