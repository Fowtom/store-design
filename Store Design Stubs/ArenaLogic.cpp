//
// Created by Tommy on 12/2/2016.
//

#include "ArenaLogic.h"
#include <ctime>
#include <cstdlib>

void ArenaLogic::determineEnemyMove()
{
    // Initialize enemy attack and defend states to false
    Enemies[0]->setenemyattackState(0);
    Enemies[0]->setenemydefendState(0);

    // Random number generator
    srand(time(NULL));

    // Random binary value is stored in variable value
    int value = rand() % 2;

    // Checks to see if the value is false
    if(value == 0)
    {
        // Sets enemy attack state to true
        Enemies[0]->setenemyattackState(1);
    }
    else
    {
        // Sets enemy defend state to true
        Enemies[0]->setenemydefendState(1);
    }

    // Enter main arena driver
    return promptArenaInput();
}

void ArenaLogic::promptArenaInput()
{
    cout << "Your opponent: " << endl << "" << endl;

    // Print out details opponent
    printEnemyTokens();

    cout << "" << endl;

    // Print out player and enemy health
    cout << "Your current health: " << thePlayer->get_playerhealth() << endl;
    cout << Enemies[0]->getEnemyName() << "'s current health: " << Enemies[0]->getEnemyHealth() << endl << "" << endl;

    // Print out main options in arena
    cout << "|----------------------------|" << endl;
    cout << "| What would you like to do? |" << endl;
    cout << "|----------------------------|" << endl;
    cout << "|        Attack: 'q'         |" << endl;
    cout << "|        Defend: 'w'         |" << endl;
    cout << "|          Heal: 'e'         |" << endl;
    cout << "|    View Stats: 'r'         |" << endl;
    cout << "|----------------------------|" << endl << "" << endl;

    // Get user input
    cin >> arenaUserInput;

    // Initialize user input validation
    bool validateInput = 0;

    // Validates user input
    while(validateInput == 0)
    {
        // If user types out one of the key letters mentioned in the instructions
        if(arenaUserInput == "q" or arenaUserInput == "w" or arenaUserInput == "e" or arenaUserInput == "r")
        {
            // Exit while loop
            validateInput = 1;
        }
        else
        {
            // Print feedback, letting user know his/her input is invalid
            cout << "Your input is invalid, please try again" << endl << "" << endl;

            // Return to store menu
            return promptArenaInput();
        }
    }

    // Each of the following conditionals calls the associated begin[task]Logic() method.

    // If user types "q"
    if(arenaUserInput == "q")
    {
        // Call beginattackLogic()
        return beginattackLogic();
    }

    // If user types "w"
    if(arenaUserInput == "w")
    {
        // Call begindefendLogic()
        return begindefendLogic();
    }

    // If user types "e"
    if(arenaUserInput == "e")
    {
        // Call beginhealLogic()
        return beginhealLogic();
    }

    // If user types "r"
    if(arenaUserInput == "r")
    {
        // Call beginprintplayerstatsLogic()
        return beginprintplayerstatsLogic();
    }
}

void ArenaLogic::beginattackLogic()
{
    // If the enemy is going to attack
    if(Enemies[0]->getenemyattackState() == 1)
    {
        // Remove player's attack from enemy's health
        Enemies[0]->removeenemyhealth(thePlayer->get_playerattackValue());

        // Remove enemy's attack from player's health
        thePlayer->removeplayerhealth(Enemies[0]->getEnemyAttackValue());

        // Print feedback to the user
        cout << Enemies[0]->getEnemyName() << " attacked!" << endl;

        // Call checkdeathStatus()
        return checkdeathStatus();
    }
    else
    {
        // Print feedback to the user
        cout << Enemies[0]->getEnemyName() << " defended!" << endl;

        // If the enemy's defend value - player's attack value is positive
        if(Enemies[0]->getEnemyDefendValue() - thePlayer->get_playerattackValue() > 0)
        {
            // Remove the enemy's defend value - the player's attack value from the enemy's health
            Enemies[0]->removeenemyhealth(Enemies[0]->getEnemyDefendValue() - thePlayer->get_playerattackValue());

            // Call checkdeathStatus()
            return checkdeathStatus();
        }
        else
        {
            // Remove 1 point of health from the enemy's health
            Enemies[0]->removeenemyhealth(1);

            // Print feedback
            cout << "Your attack is very weak against " << Enemies[0]->getEnemyName() << "!" << endl << "" << endl;

            // Call checkdeathStatus()
            return checkdeathStatus();
        }
    }
}

void ArenaLogic::begindefendLogic()
{
    // Check if enemy defended
    if(Enemies[0]->getenemydefendState() == 1)
    {
        // Print feedback
        cout << "Both characters defended!" << endl;

        // Call checkdeathStatus()
        return checkdeathStatus();
    }
    else
    {
        // Print feedback to the user
        cout << Enemies[0]->getEnemyName() << " attacked!" << endl;

        // Check if enemy attack value - player defend value is positive
        if(Enemies[0]->getEnemyAttackValue() - thePlayer->get_playerdefendValue() > 0)
        {
            // Remove this value from the player's health
            thePlayer->removeplayerhealth(Enemies[0]->getEnemyAttackValue() - thePlayer->get_playerdefendValue());

            // Call checkdeathStatus()
            return checkdeathStatus();
        }
        else
        {
            // Remove 1 point of health from the player's health
            thePlayer->removeplayerhealth(1);

            // Print feedback
            cout << "Your opponent barely hit you!" << endl;

            // Call checkdeathStatus()
            return checkdeathStatus();
        }
    }
}

void ArenaLogic::beginhealLogic()
{
    // Initialize variable for checking item type
    bool checkItem = 0;

    // While loop for checking the item
    while (checkItem == 0)
    {
        // For loop that iterates through the player's bag
        for (long i = 0 ; i < thePlayer->get_bag().size() ; i++)
        {
            // If any item in bag is a health item
            if (thePlayer->get_bag()[i]->get_type() == "Health")
            {
                // Exit while loop
                checkItem = 1;
                break;
            }
        }
        // If still in while loop
        if(checkItem == 0)
        {
            // Print feedback
            cout << "You don't have any items in your bag that can be used for healing!" << endl;

            // Call promptArenaInput()
            return promptArenaInput();
        }
    }

    // For loop that iterates through player's bag
    for(long i = 0 ; i < thePlayer->get_bag().size() ; i++)
    {
        // If there are any health items in bag
        if(thePlayer->get_bag()[i]->get_type() == "Health")
        {
            // Print out health items in bag
            cout << thePlayer->get_bag()[i]->get_name() << endl;
        }
    }

    // Print feedback for what the user
    cout <<"What item would you like to use?" << endl;
    cout << "Enter 'c' to cancel" << endl;

    // Initialize input validator
    bool validateInput = 0;

    // While loop to validate user input
    while(validateInput == 0)
    {
        // Get input
        cin >> arenaUserInput;

        // Iterate through the size of the bag
        for (int i = 0; i < thePlayer->get_bag().size(); i++)
        {
            // Set variable "x" to the name of the item in the bag
            string x = thePlayer->get_bag()[i]->get_name();

            // Checks to see if user typed in the name of the item
            if (arenaUserInput == x)
            {
                // If that item type is "Armor"
                if(thePlayer->get_bag()[i]->get_type() == "Armor")
                {
                    // Print feedback
                    cout << "This item cannot be used!" << endl;

                    // call promptArenaInput()
                    return promptArenaInput();
                }
                // Check if item type is "Weapon"
                else if(thePlayer->get_bag()[i]->get_type() == "Weapon")
                {
                    // Print feedback
                    cout << "This item cannot be used!" << endl;

                    // call promptArenaInput()
                    return promptArenaInput();
                }
                // Otherwise
                else
                {
                    // Exit the while loop
                    validateInput = 1;
                    break;
                }
            }
            // If the user wants to cancel
            if(arenaUserInput == "c")
            {
                // Call promptArenaInput()
                return promptArenaInput();
            }
        }
        // If still in while loop
        if(validateInput == 0)
        {
            // Return error feedback
            cout << "Your input was invalid, please try again" << endl;
        }
    }

    // Iterate through size of bag
    for (int i = 0 ; i < thePlayer->get_bag().size(); i++)
    {
        // If the item in bag matches user input
        if(thePlayer->get_bag()[i]->get_name() == arenaUserInput)
        {
            // Create instance of that item
            BaseSalableItem x = *thePlayer->get_bag()[i];

            // Add stat value to player's health
            thePlayer->addplayerhealth(x.get_stat());

            // If the player's max health is exceeded
            if(thePlayer->get_playerhealth() > 500)
            {
                // Set to max
                thePlayer->setplayerhealth(500);

                // Print feedback
                cout << "Your player has been fully healed!" << endl;
            }
            // Otherwise
            else
            {
                // Print how much health has been restored from the item
                cout << "You have restored " << x.get_stat() << " health from " << arenaUserInput << endl;
            }

            // Remove the item from the bag
            thePlayer->removefromBag(i);
            break;
        }
    }

    // If the enemy defended
    if(Enemies[0]->getenemyattackState() == 0)
    {
        // Print feedback to the user
        cout << Enemies[0]->getEnemyName() << " defended!" << endl;

        // Call checkdeathStatus()
        return checkdeathStatus();
    }
    // Otherwise
    else
    {
        // Print feedback to the user
        cout << Enemies[0]->getEnemyName() << " attacked!" << endl;

        // Remove the enemy's attack value from the player's health
        thePlayer->removeplayerhealth(Enemies[0]->getEnemyAttackValue());

        // Call checkdeathStatus()
        return checkdeathStatus();
    }
}

void ArenaLogic::beginprintplayerstatsLogic()
{
    // If the player does not have any items equipped
    if(thePlayer->get_equippedItems().size() == 0)
    {
        // Print stats out anyways
        thePlayer->printplayerStats();

        // Print feedback
        cout << "You're defenseless! Equip a weapon and some armor to defend yourself" << endl << "" << endl;

        // Call promptArenaInput()
        return promptArenaInput();
    }
    else
    {
        // Print out the player's stats
        thePlayer->printplayerStats();

        // Call promptArenaInput()
        return promptArenaInput();
    }
}

void ArenaLogic::checkdeathStatus()
{
    // If the player is dead
    if(thePlayer->get_playerhealth() <= 0)
    {
        // Do nothing (to be continued... check
        // beginArenaLogic()
    }
    // Check if the enemy is dead
    else if(Enemies[0]->getEnemyHealth() <= 0)
    {
        // Print feedback letting user know he/she won
        cout << "Congratulations! " << Enemies[0]->getEnemyName() << " has been defeated!" << endl;

        // Add the enemy's balance to the player's balance
        thePlayer->addtoplayerBalance(Enemies[0]->getEnemyBalance());

        // Print out how much gold has been added
        cout << Enemies[0]->getEnemyBalance() << " gold has been added to your balance!" << endl;

        // Restore player to full health
        thePlayer->setplayerhealth(500);

        cout << "You have been restored to full health!" << endl;

        // Remove enemy from Arena enemies vector
        Enemies.erase(Enemies.begin() + 0);

        // Check if all enemies have been defeated
        if(checkEnemiesSize() == 0)
        {
            // Do nothing (to be continued... check
            // beginArenaLogic()
        }
        else
        {
            // Print some options for the player
            cout << "Would you like to keep fighting or leave?" << endl;
            cout << "Type 'leave' to exit the arena or any key to fight the next boss" << endl;

            // Initialize input validator
            bool validateInput = 0;

            // Validates input
            while(validateInput == 0)
            {
                // Gets user input
                cin >> arenaUserInput;

                // If the user types "leave"
                if(arenaUserInput.compare("leave") == 0)
                {
                    // Prints "Thanks for playing!"
                    cout << "Thanks for playing!" << endl;

                    // Exits the while loop
                    validateInput = 1;
                }
                else
                {
                    // Call determineEnemyMove(), start next fight
                    return determineEnemyMove();
                }
            }
        }
    }
    else
    {
        // Call determineEnemyMove(), start next fight
        return determineEnemyMove();
    }
}