//
// Created by Tommy on 12/2/2016.
//

#ifndef STORE_DESIGN_STUBS_GAME_H
#define STORE_DESIGN_STUBS_GAME_H
#include <iostream>
#include "StoreLogic.h"
#include "ArenaLogic.h"

using namespace std;

class Game
{
    protected:
        StoreLogic *theStoreLogic = new StoreLogic;
        ArenaLogic *theArenaLogic = new ArenaLogic;
        Player* thePlayer = Player::instance();

    public:
        /**
         * Used to save progress. Information will be saved to a text file that will be
         * opened and parsed through upon startup and will initialize member variables.
         */
        void save();

        /**
         * Destructor for Game. Deletes all instances of StoreLogic, ArenaLogic, and Player.
         * Gets executed at the end of the program
         */
        ~Game();

        int loadfile();

        int loaddefaultfile();

};


#endif //STORE_DESIGN_STUBS_GAME_H
