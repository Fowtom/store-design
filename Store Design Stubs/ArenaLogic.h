//
// Created by Tommy on 12/2/2016.
//

#ifndef STORE_DESIGN_STUBS_ARENALOGIC_H
#define STORE_DESIGN_STUBS_ARENALOGIC_H
#include <iostream>
#include "Arena.h"
using namespace std;

class ArenaLogic : public Arena
{
    private:
        string arenaUserInput;

    public:
        /**
         * Generates a number to determine if the enemy will attack or defend.
         * @return true or false
         */
        void determineEnemyMove();

        /**
         * This is the main driver method for the arena that allows the user
         * to decide what they would like to do: Attack, defend, heal or check
         * their stats. Before doing so, it will print out the player and enemy's
         * current health. The inputs for each are "a", "s", "d", and "f", respectively.
         * Using the input, this method will call the begin[task]Logic() method
         * associated with the input.
         */
        void promptArenaInput();

        /**
         * This method is used for checking the player's stats. All it does is print
         * out the player's stats and call promptArenaInput().
         */
        void beginprintplayerstatsLogic();

        /**
         * This method is called when the user decides to attack. It starts off by
         * checking the value that was determined in determineEnemyMove(). It will
         * then proceed with any calculations that will be used if the enemy attacked
         * or defended. After this, it will call checkdeathStatus() and then
         * promptArenaInput()
         */
        void beginattackLogic();

        /**
         * This method is called when the user decides to defend. It starts off by
         * checking the value that was determined in determineEnemyMove(). It will
         * then proceed with any calculations that will be used if the enemy attacked
         * or defended. After this, it will call checkdeathStatus() and then
         * promptArenaInput()
         */
        void begindefendLogic();

        /**
         * This method is called when the user decides to heal. It starts off by
         * checking if the player's bag is empty, and if there are any healing items
         * in the player's bag. It will then prompt the user for what healing item they
         * would like to consume, and then add the health value of the item to the player's
         * health and call promptArenaInput().
         */
        void beginhealLogic();

        /**
         * This method is used to check if either the player or the enemy has been
         * defeated. If the player is defeated, it will give feedback to the user and
         * exit the arena. If the enemy is defeated, the player will receive gold as
         * a prize and the player will be fully healed. The player can then decide if
         * they would like to keep fighting or leave the arena.
         */
        void checkdeathStatus();
};


#endif //STORE_DESIGN_STUBS_ARENALOGIC_H
