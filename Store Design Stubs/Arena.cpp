//
// Created by Tommy on 12/2/2016.
//

#include "Arena.h"
using namespace std;

Arena :: ~Arena()
{
    // For loop to iterate through size of Enemies
    for(int i = 0 ; i < Enemies.size() ; i++)
    {
        // Destroy all enemies
        delete Enemies[i];
    }
}

bool Arena::checkEnemiesSize()
{
    // If Enemies size > 0
    if(Enemies.size() > 0)
    {
        // Return 1
        return 1;
    }
    else
    {
        // Return 0
        return 0;
    }
}

void Arena::addEnemy(Enemy* enemy)
{
    // Add enemy instances to Enemies vector
    Enemies.push_back(enemy);
}

void Arena::printEnemyTokens()
{
    // Prints out the name, health, attack, defense, and description tokens for each enemy
    cout << Enemies[0]->getEnemyName() << endl;
    cout << "   Health: " << Enemies[0]->getEnemyHealth() << endl;
    cout << "   Attack: " << Enemies[0]->getEnemyAttackValue() << endl;
    cout << "   Defense: " << Enemies[0]->getEnemyDefendValue() << endl;
    cout << "   Reward: " << Enemies[0]->getEnemyBalance() << " gold" << endl;
}

vector <Enemy*> Arena::get_Enemies()
{
    // Returns the Enemies vector
    return Enemies;
}