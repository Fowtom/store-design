//
// Created by Tommy on 11/3/2016.
//

#include <iostream>
#include "Store.h"
using namespace std;

bool Store::checkCartSize()
{
    // If the cart has something in it
    if (Cart.size() > 0)
    {
        // Return true
        return 1;
    }
    else
    {
        // Return false
        return 0;
    }
}

void Store::printCart()
{
    // Iterate through size of cart
    for(int i = 0 ; i < Cart.size() ; i++)
    {
        // Print list of items in cart.
        cout << "   " << Cart[i]->get_name() << endl;
    }
}

void Store::printCartItemTokens()
{
    // Iterate through size of cart
    for(int i = 0 ; i < Cart.size() ; i++)
    {
        // Print out name, cost, stat, and description tokens for each item in cart
        cout << Cart[i]->get_name() << endl;
        cout << "   Type: " << Cart[i]->get_type() << endl;
        cout << "   Cost: " << Cart[i]->get_cost() << " gold" << endl;
        cout << "   Stat: +" << Cart[i]->get_stat() << endl;
        cout << "   Description: " << Cart[i]->get_description() << endl << "" << endl;
    }
}

Store::~Store()
{
    // Iterate through size of cart
    for(int i = 0 ; i < Cart.size() ; i++)
    {
        // Destroy all items in cart
        delete Cart[i];
    }
}

vector <BaseSalableItem*> Store::get_cart()
{
    // Returns the cart
    return Cart;
}

int Store::get_cartBalance()
{
    return cartBalance;
}

void Store::setcartBalance(int gold)
{
    cartBalance = gold;
}
