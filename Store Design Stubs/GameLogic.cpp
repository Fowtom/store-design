//
// Created by Tommy on 12/2/2016.
//

#include "GameLogic.h"
#include <fstream>

void GameLogic::beginstartLogic()
{
    // Print feedback and give the user some options
    cout << "Welcome to the Fantasy Fighting Game!" << endl;
    cout << "Would you like to load a file, or start a new game?" << endl;
    cout << "Please type either 'load' or 'start'" << endl;

    // Initialize input validator
    bool validateInput = 0;

    // Validates user input
    while(validateInput == 0)
    {
        // Get the user input
        cin >> gameUserInput;

        // If user types out one of the key letters mentioned in the instructions
        if(gameUserInput == "load")
        {
            // Open save file
            ifstream saveFile;

            try
            {
                // Open save file
                saveFile.open("./Save.txt", ios::in);

                // If the save file cannot be opened
                if(saveFile.peek() == ifstream::traits_type::eof())
                {
                    // Close the file
                    saveFile.close();

                    // Start new file
                    loaddefaultfile();

                    // Throw exception indicating the file cannot be opened
                    throw invalid_argument("Save file does not exist, loading a new one...");
                }
                else
                {
                    // Load save file
                    loadfile();

                    return promptGameInput();
                }
            }
            catch(invalid_argument x)
            {
                // Print exception message
                cout << x.what() << endl;
                return promptGameInput();
            }
        }
        else if(gameUserInput == "start")
        {
            validateInput = 1;
        }
        else
        {
            // Print feedback, letting user know his/her input is invalid
            cout << "Your input was invalid, please try again" << endl << "" << endl;
        }
    }

    // Load everything in its default state
    loaddefaultfile();

    // Ask user for their name
    cout << "What is your name?" << endl;
    cin >> gameUserInput;

    // Set their input as their name
    thePlayer->setplayername(gameUserInput);

    // Call main game driver
    return promptGameInput();
}

void GameLogic::promptGameInput()
{
    // Print instructions for all the available options the user can work with.
    // These instructions include the key letters associated with each option. For
    // example: q = "Go to Store", w = "Go to Arena", e = "Equip an Item", r = "Unequip
    // an item", t = "View stats", y = "Exit".
    cout << "/---------------------------------------/" << endl;
    cout << "/ Welcome to the Fantasy Fighting Game! /" << endl;
    cout << "/---------------------------------------/" << endl;
    cout << "/     Press 'q' to 'Go to Store'        /" << endl;
    cout << "/     Press 'w' to 'Go to Arena'        /" << endl;
    cout << "/     Press 'e' to 'Equip an Item'      /" << endl;
    cout << "/     Press 'r' to 'Unequip an Item'    /" << endl;
    cout << "/     Press 't' to 'View Stats'         /" << endl;
    cout << "/     Press 'y' to 'Exit'               /" << endl;
    cout << "/---------------------------------------/" << endl << "" << endl;

    // Get user input
    cin >> gameUserInput;

    // Initialize user input validation
    bool validateInput = 0;

    // Validates user input
    while(validateInput == 0)
    {
        // If user types out one of the key letters mentioned in the instructions
        if(gameUserInput == "q" or gameUserInput == "w" or gameUserInput == "e" or gameUserInput == "r" or gameUserInput == "t" or gameUserInput == "y")
        {
            // Exit while loop
            validateInput = 1;
        }
        else
        {
            // Print feedback, letting user know his/her input is invalid
            cout << "Your input is invalid, please try again" << endl << "" << endl;

            // Return to store menu
            return promptGameInput();
        }
    }

    // Each of the following conditionals calls the associated begin[task]Logic() method.

    // If user types "q"
    if(gameUserInput == "q")
    {
        // Call beginStoreLogic()
        return beginStoreLogic();
    }

    // If user types "w"
    if(gameUserInput == "w")
    {
        // Call beginArenaLogic()
        return beginArenaLogic();
    }

    // If user types "e"
    if(gameUserInput == "e")
    {
        // Call beginEquipLogic()
        return beginEquipLogic();
    }

    // If user types "r"
    if(gameUserInput == "r")
    {
        // Call beginUnequipLogic()
        return beginUnequipLogic();
    }

    // If user types "t"
    if(gameUserInput == "t")
    {
        // Call beginStatsLogic()
        return beginStatsLogic();
    }

    // If user types "y"
    if(gameUserInput == "y")
    {
        // Call beginExitLogic()
        return beginExitLogic();
    }
}

void GameLogic::beginStoreLogic()
{
    // Call promptStoreInput() for Store.
    theStoreLogic->promptStoreInput();

    // Save progress
    save();

    // Go back to the game driver
    return promptGameInput();
}

void GameLogic::beginArenaLogic()
{
    // Call determineEnemyMove()
    theArenaLogic->determineEnemyMove();

    // Check if player is defeated
    if(thePlayer->get_playerhealth() <= 0)
    {
        // Print feedback and exit code
        cout << "You have been defeated!" << endl;
        save();
    }
    // Check if there are still enemies to fight
    else if(theArenaLogic->checkEnemiesSize() == 1)
    {
        // Call promptGameInput()
        return promptGameInput();
    }
    else
    {
        // Print feedback letting user know he/she beat the game and exit the code
        cout << "Congratulations " << thePlayer->get_name() << ", you have defeated every enemy in the arena!" << endl;
        cout << "Thanks for playing!" << endl;
    }
}

void GameLogic::beginEquipLogic()
{
    // Initialize item check variable
    bool checkItem = 0;

    // Check item in while loop
    while (checkItem == 0)
    {
        // Iterate through the size of the bag
        for (long i = 0 ; i < thePlayer->get_bag().size() ; i++)
        {
            // If any item in bag is of type "Armor"
            if (thePlayer->get_bag()[i]->get_type() == "Armor")
            {
                // Exit while loop
                checkItem = 1;
                break;
            }
            // If any item in bag is of type "Weapon"
            else if (thePlayer->get_bag()[i]->get_type() == "Weapon")
            {
                // Exit while loop
                checkItem = 1;
                break;
            }
        }

        // If still in while loop
        if(checkItem == 0)
        {
            // Print feedback letting user know he/she cannot equip any items
            cout << "You don't have any items in your bag that can be equipped!" << endl << "" << endl;

            // Call promptGameInput()
            return promptGameInput();
        }
    }

    // Print feedback
    cout << "Your equippable items: " << endl << "" << endl;

    // Iterate through the size of the player's bag
    for(long i = 0 ; i < thePlayer->get_bag().size() ; i++)
    {
        // If there are any weapons in the bag
        if(thePlayer->get_bag()[i]->get_type() == "Weapon")
        {
            // Print out the names of the weapons
            cout << "   " << thePlayer->get_bag()[i]->get_name() << endl;
        }
        // If there is any armor in the bag
        else if(thePlayer->get_bag()[i]->get_type() == "Armor")
        {
            // Print out the names of the armor
            cout << "   " << thePlayer->get_bag()[i]->get_name() << endl;
        }
    }

    cout << "" << endl;

    // Print feedback to the user
    cout <<"What would you like to equip?" << endl;
    cout << "Enter 'c' to cancel" << endl;

    // Initialize input validator
    bool validateInput = 0;

    // While loop that validates user input
    while(validateInput == 0)
    {
        // Get user input
        cin >> gameUserInput;

        // Iterate through the size of the bag
        for (int i = 0; i < thePlayer->get_bag().size(); i++)
        {
            // Set variable "x" to the name of the item in the bag
            string x = thePlayer->get_bag()[i]->get_name();

            // Checks to see if user typed in the name of the item
            if (gameUserInput == x)
            {
                // If the item is of type "Health"
                if(thePlayer->get_bag()[i]->get_type() == "Health")
                {
                    // Print feedback to the user
                    cout << "This item cannot be equipped!" << endl;

                    // Call promptGameInput()
                    return promptGameInput();
                }

                // Exit the while loop
                validateInput = 1;
                break;
            }
            // If user decides to cancel
            if(gameUserInput == "c")
            {
                // Call promptGameInput()
                return promptGameInput();
            }
        }

        // If still in while loop
        if(validateInput == 0)
        {
            // Return error
            cout << "Your input was invalid" << endl;
        }
    }

    // Iterate through size of bag
    for (int i = 0 ; i < thePlayer->get_bag().size(); i++)
    {
        // If the name matches the user input
        if(thePlayer->get_bag()[i]->get_name() == gameUserInput)
        {
            // Create instance of BaseSalableItem for each item in bag
            BaseSalableItem *x = thePlayer->get_bag()[i];

            // Remove item from bag
            thePlayer->removefromBag(i);

            // Add item to equippedItems vector
            thePlayer->addtoequippedItems(x);

            // If the item is of type "Weapon"
            if(x->get_type() == "Weapon")
            {
                // Add the stat of the weapon to the player's attack value
                thePlayer->addplayerattackValue(x->get_stat());
            }
            else
            {
                // Add the stat of the armor to the player's defend value
                thePlayer->addplayerdefendValue(x->get_stat());
            }

            // Print feedback letting user know his/her item has been equipped
            cout << gameUserInput << " has been equipped!" << endl;
            break;
        }
    }

    // Print out feedback
    cout << "You have equipped the following items: " << endl << "" << endl;

    // Print equippedItems vector
    thePlayer->printequippedItems();

    cout << "" << endl;

    // Checks the size of the bag
    if(thePlayer->checkBagSize() == 0)
    {
        // Prints feedback
        cout << "Your bag is now empty!" << endl;
    }
    else
    {
        // Prints feedback
        cout << "The contents of your bag: " << endl << "" << endl;

        // Print out the contents in the user's bag
        thePlayer->printBag();

        cout << "" << endl;
    }

    // Call promptGameInput()
    return promptGameInput();
}

void GameLogic::beginUnequipLogic()
{
    // If the size of the player's equippedItems vector is 0
    if(thePlayer->get_equippedItems().size() == 0)
    {
        // Print feedback
        cout << "You don't have any items equipped!" << endl;

        // Call promptGameInput()
        return promptGameInput();
    }

    // Print feedback
    cout << "You have equipped the following items: " << endl << "" << endl;

    // Print list of equipped items
    thePlayer->printequippedItems();

    // Print feedback prompting for input
    cout << "" << endl << "What would you like to unequip?" << endl;
    cout << "Enter 'c' to cancel" << endl;

    // Initialize input validator
    bool validateInput = 0;

    // While loop that validates input
    while(validateInput == 0)
    {
        // Get user input
        cin >> gameUserInput;

        // Iterate through the size of the equippedItems vector
        for (int i = 0; i < thePlayer->get_equippedItems().size(); i++)
        {
            // Set variable "x" to the name of the item in equippedItems
            string x = thePlayer->get_equippedItems()[i]->get_name();

            // Checks to see if user typed in the name of the item
            if (gameUserInput == x)
            {
                // Exit the while loop
                validateInput = 1;
            }
            // If user decides to cancel
            if(gameUserInput == "c")
            {
                // Call promptGameInput()
                return promptGameInput();
            }
        }
        // If still in while loop
        if(validateInput == 0)
        {
            // Return error
            cout << "Your input was invalid" << endl;
        }
    }

    // For loop that iterates through the size of equippedItems
    for (int i = 0 ; i < thePlayer->get_equippedItems().size(); i++)
    {
        // If the input matches the item in equippedItems
        if(thePlayer->get_equippedItems()[i]->get_name() == gameUserInput)
        {
            // Create BaseSalableItem instance for that item
            BaseSalableItem *x = thePlayer->get_equippedItems()[i];

            // Unequip that item
            thePlayer->removeequippedItems(i);

            // Add the item to the player's bag
            thePlayer->addtoBag(x);

            // If the item is a weapon
            if(x->get_type() == "Weapon")
            {
                // Remove the weapon's attack value from the player's attack value
                thePlayer->removeplayerattackValue(x->get_stat());
            }
            else
            {
                // Remove the armor's defend value from the player's defend value
                thePlayer->removeplayerdefendValue(x->get_stat());
            }

            // Print feedback letting the user know his/her item has been unequipped
            cout << gameUserInput << " has been unequipped and added to your bag!" << endl << "" << endl;
            break;
        }
    }

    // Call promptGameInput()
    return promptGameInput();
}

void GameLogic::beginStatsLogic()
{
    // Check if the equippedItems vector is empty
    if(thePlayer->get_equippedItems().size()== 0)
    {
        // Print out the player's stats
        thePlayer->printplayerStats();

        // Print feedback letting the user know why his/her stats are so low
        cout << "You're defenseless! Equip a weapon and some armor to defend yourself" << endl << "" << endl;

        // Call promptGameInput
        return promptGameInput();
    }
    else
    {
        // Print out the player's stats
        thePlayer->printplayerStats();

        // Call promptGameInput()
        return promptGameInput();
    }
}

void GameLogic::beginExitLogic()
{
    // Print feedback
    cout << "Thanks for playing!" << endl;

    // Call save() from Game
    save();

    // Exit code
}