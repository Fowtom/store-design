//
// Created by Tommy on 11/3/2016.
//

#include <iostream>
#include "Player.h"

// Initialize all static member variables
Player* Player::inst = 0;
string Player::name = "Thrall"; // Thrall is the name of the Shaman hero from Hearthstone
int Player::playerBalance = 500;
int Player::health = 500;
int Player::playerAttackValue = 15;
int Player::playerDefendValue = 15;
vector <BaseSalableItem*> Player::bag;
vector <BaseSalableItem*> Player::equippedItems;


Player::Player()
{
    // Do nothing
}

Player* Player::instance()
{
    // If a player instance does not exist
    if(!inst)
    {
        // Set instance as new Player
        inst = new Player();
    }

    // Return the instance
    return inst;
}

vector <BaseSalableItem*> Player::get_bag()
{
    // Returns the user's bag
    return bag;
}

vector <BaseSalableItem*> Player::get_equippedItems()
{
    // Returns the user's equippedItems
    return equippedItems;
}

int Player::get_playerBalance()
{
    // Returns the user's current balance
    return playerBalance;
}

void Player::printBag()
{
    // Iterates through size of bag
    for(int i = 0 ; i < bag.size() ; i++)
    {
        // Prints out the name of each item in bag
        cout << "   " << bag[i]->get_name() << endl;
    }
}

void Player::addtoplayerBalance(int gold)
{
    // Add gold to user's balance
    playerBalance += gold;
}


void Player::removefromplayerBalance(int gold)
{
    // Remove gold from user's balance
    playerBalance -= gold;
}

void Player::addtoBag(BaseSalableItem *item)
{
    // Push_back method adds salable item to bag vector
    bag.push_back(item);
}

void Player::removefromBag(int item)
{
    // Swaps the position of the selected item to the back of the bag
    swap(bag[item], bag.back());

    // Removes the selected item from the bag
    bag.pop_back();
}

bool Player::checkBagSize()
{
    // Check to see if bag has something in it
    if (bag.size() > 0)
    {
        // Return true
        return 1;
    }
    else
    {
        // Return false
        return 0;
    }
}

Player::~Player()
{
    // Iterate through size of the bag
    for(int i = 0 ; i < bag.size() ; i++)
    {
        // Destroy all bag contents
        delete bag[i];
    }

    for(int i = 0 ; i < equippedItems.size() ; i++)
    {
        delete equippedItems[i];
    }
}

void Player::printequippedItems()
{
    // Iterates through size of equippedItems
    for(int i = 0 ; i < equippedItems.size() ; i++)
    {
        // Prints out the name of each item in equippedItems
        cout << "   " << equippedItems[i]->get_name() << endl;
    }
}

void Player::addtoequippedItems(BaseSalableItem *item)
{
    // Push_back method adds salable item to equippedItems vector
    equippedItems.push_back(item);
}

void Player::addplayerattackValue(int stat)
{
    // Adds parameter to playerAttackValue
    playerAttackValue += stat;
}

void Player::addplayerdefendValue(int stat)
{
    // Adds parameter to playerDefendValue
    playerDefendValue += stat;
}

void Player::removeequippedItems(int item)
{
    // Swaps the position of the selected item to the back of equippedItems
    swap(equippedItems[item], equippedItems.back());

    // Pop_back() method removes item from equippedItems vector
    equippedItems.pop_back();
}

void Player::removeplayerattackValue(int stat)
{
    // Subtracts parameter from playerAttackValue
    playerAttackValue -= stat;
}

void Player::removeplayerdefendValue(int stat)
{
    // Subtracts parameter from playerDefendValue
    playerDefendValue -= stat;
}

void Player::printplayerStats()
{
    // Prints out player name, health, attackValue, and defendValue
    cout << "Name: " << name << endl;
    cout << "Health: " << health << endl;
    cout << "Attack: " << playerAttackValue << endl;
    cout << "Defense: " << playerDefendValue << endl;
    cout << "" << endl;
}

void Player::addplayerhealth(int stat)
{
    // Adds parameter to player's health
    health += stat;
}

void Player::removeplayerhealth(int stat)
{
    // Subtracts parameter from player's health
    health -= stat;
}

int Player::setplayerhealth(int stat)
{
    // Sets parameter equal to player's health
    health = stat;
}

int Player::get_playerhealth()
{
    // Return player's health
    return health;
}

int Player::get_playerattackValue()
{
    // Return player's attack value
    return playerAttackValue;
}

int Player::get_playerdefendValue()
{
    // Return player's defend value
    return playerDefendValue;
}

string Player::get_name()
{
    // Return name of player
    return name;
}

void Player::setplayername(string playername)
{
    name = playername;
}

void Player::setplayerattackValue(int stat)
{
    playerAttackValue = stat;
}

void Player::setplayerdefendValue(int stat)
{
    playerDefendValue = stat;
}

void Player::setplayerBalance(int gold)
{
    playerBalance = gold;
}

void Player::addtobag(BaseSalableItem *item)
{
    // Push_back method adds salable item to equippedItems vector
    bag.push_back(item);
}