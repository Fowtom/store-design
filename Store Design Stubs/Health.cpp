//
// Created by Tommy on 11/3/2016.
//

#include "Health.h"

Health::Health(string name, string type, string description, int cost, int stat)
{
    // Nondefault constructor that sets the values of each of the member variables
    this->name = name;
    this->type = type;
    this->description = description;
    this->cost = cost;
    this->stat = stat;
}
