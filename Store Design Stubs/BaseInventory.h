//
// Created by Tommy on 11/3/2016.
//

#ifndef STORE_DESIGN_STUBS_INVENTORY_H
#define STORE_DESIGN_STUBS_INVENTORY_H

#include "vector"
#include "BaseSalableItem.h"
using namespace std;

class BaseInventory
{
    protected:
        vector <BaseSalableItem*> ItemList;

    public:

        /**
         * Destructor for BaseInventory class. Gets called
         * when the program has finished. The destructor deletes
         * all the contents of the inventory
         */
        ~BaseInventory();

        /**
         * Iterates through the size of the inventory and prints
         * out the name of each item in the inventory vector
         */
        void printItemList();

        /**
         * Iterates through the size of the inventory and prints
         * out the name, type, cost, stat, and description of
         * each item in the inventory vector
         */
        void printInventoryItemTokens();

        /**
         * Adds an item to the inventory vector via push_back
         * @param item of type pointer BaseSalableItem
         */
        void addSalableItem(BaseSalableItem* item);

        /**
         * Checks the size of the inventory
         * @return True (1) if the inventory has something in it.
         * If the inventory is empty, return false (0).
         */
        bool checkInventorySize();

        /**
         * Getter method for the ItemList, used for saving and loading purposes
         * @return ItemList
         */
        vector <BaseSalableItem*> get_ItemList();
};

#endif //STORE_DESIGN_STUBS_INVENTORY_H
