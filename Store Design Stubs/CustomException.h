//
// Created by Tommy on 12/2/2016.
//

#ifndef STORE_DESIGN_STUBS_EXCEPTION_H
#define STORE_DESIGN_STUBS_EXCEPTION_H
#include <iostream>
using namespace std;

class CustomException
{
    protected:
        string msg = "The file cannot be read";

    public:

        /**
         * Method that determines what exception will be used
         * @return msg
         */
        const char* what() const throw();
};


#endif //STORE_DESIGN_STUBS_EXCEPTION_H
