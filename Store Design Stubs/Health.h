//
// Created by Tommy on 11/3/2016.
//

#ifndef STORE_DESIGN_STUBS_HEALTH_H
#define STORE_DESIGN_STUBS_HEALTH_H

#include "BaseSalableItem.h"

class Health : public BaseSalableItem
{
    public:

        /**
         * Nondefault constructor for health
         * @param name Name of the health item
         * @param type Type of the health item
         * @param description Description of the health item
         * @param cost Cost of the health item
         * @param stat Stat value of the health item
         * @return Nothing
         */
        Health(string name, string type, string description, int cost, int stat);
};


#endif //STORE_DESIGN_STUBS_HEALTH_H
