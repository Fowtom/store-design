//
// Created by Tommy on 11/3/2016.
//

#include <iostream>
#include "BaseInventory.h"

using namespace std;

void BaseInventory::printItemList()
{
    // Iterate through size of inventory
    for(int i = 0 ; i < ItemList.size() ; i++)
    {
        // Print list of items in inventory.
        cout << ItemList[i]->get_name() << endl;
    }
}

void BaseInventory::printInventoryItemTokens()
{
    // Iterates throught the size of the inventory
    for(int i = 0 ; i < ItemList.size() ; i++)
    {
        // Prints out the name, cost, stat, and description tokens for each item
        cout << ItemList[i]->get_name() << endl;
        cout << "   Type: " << ItemList[i]->get_type() << endl;
        cout << "   Cost: " << ItemList[i]->get_cost() << " gold" << endl;
        cout << "   Stat: +" << ItemList[i]->get_stat() << endl;
        cout << "   Description: " << ItemList[i]->get_description() << endl << "" << endl;
    }
}

void BaseInventory::addSalableItem(BaseSalableItem* item)
{
    // Push method that adds item to inventory.
    ItemList.push_back(item);
}

bool BaseInventory::checkInventorySize()
{
    // If the inventory has something in it
    if (ItemList.size() > 0)
    {
        // Return true
        return 1;
    }
    else
    {
        // Return false
        return 0;
    }
}

BaseInventory::~BaseInventory()
{
    // Iterate through the size of the inventory
    for(int i = 0 ; i < ItemList.size() ; i++)
    {
        // Destroy the inventory
        delete ItemList[i];
    }
}

vector <BaseSalableItem*> BaseInventory::get_ItemList()
{
    // Returns the inventory
    return ItemList;
}