//
// Created by Tommy on 12/9/2016.
//

#include <iostream>
#include "BaseSalableItem.h"
#include "Armor.h"
#include "Weapon.h"
#include "Health.h"

BaseSalableItem* BaseSalableItem::create(string name, string type, string description, int cost, int stat)
{
    // If the item is of type "Weapon"
    if(type == "Weapon")

        // Return new instance of Weapon
        return new Weapon(name, type, description, cost, stat);

    // If the item is of type "Armor"
    else if(type == "Armor")

        // Return new instance of Armor
        return new Armor(name, type, description, cost, stat);

    // If the item is of type "Health"
    else if(type == "Health")

        // Return new instance of Health
        return new Health(name, type, description, cost, stat);

    // Return NULL
    return NULL;
}