//
// Created by Tommy on 11/3/2016.
//

#ifndef STORE_DESIGN_STUBS_ARMOR_H
#define STORE_DESIGN_STUBS_ARMOR_H

#include "BaseSalableItem.h"

class Armor : public BaseSalableItem
{
    public:

        /**
         * Nondefault constructor for Armor
         * @param name Name of the armor item
         * @param type Type of the armor item
         * @param description Description of the armor item
         * @param cost Cost of the armor item
         * @param stat Stat value of the armor item
         * @return Nothing
         */
        Armor(string name, string type, string description, int cost, int stat);
};


#endif //STORE_DESIGN_STUBS_ARMOR_H
