//
// Created by Tommy on 12/2/2016.
//

#include "Enemy.h"

Enemy::Enemy(string enemyName, int enemyHealth, int enemyAttackValue, int enemyDefendValue, int enemyBalance)
{
    // Initialize enemyHealth
    this->enemyHealth = enemyHealth;

    // Initialize enemyAttackValue
    this->enemyAttackValue = enemyAttackValue;

    // Initialize enemyDefendValue
    this->enemyDefendValue = enemyDefendValue;

    // Initialize enemyBalance
    this->enemyBalance = enemyBalance;

    // Initialize enemyName
    this->enemyName = enemyName;
}

int Enemy::getEnemyHealth()
{
    // return enemyHealth
    return enemyHealth;
}

int Enemy::getEnemyAttackValue()
{
    // return enemyAttackValue
    return enemyAttackValue;
}

int Enemy::getEnemyDefendValue()
{
    // return enemyDefendValue
    return enemyDefendValue;
}

int Enemy::getEnemyBalance()
{
    // return enemyBalance
    return enemyBalance;
}

string Enemy::getEnemyName()
{
    // return enemyName
    return enemyName;
}

void Enemy::setenemyattackState(bool value)
{
    // Set attackState equal to parameter
    attackState = value;
}

void Enemy::setenemydefendState(bool value)
{
    // Set defendState equal to parameter
    defendState = value;
}

bool Enemy::getenemyattackState()
{
    // return attackState
    return attackState;
}

bool Enemy::getenemydefendState()
{
    // return defendState
    return defendState;
}

void Enemy::removeenemyhealth(int value)
{
    // Negatively iterate the enemyHealth by the parameter
    enemyHealth -= value;
}