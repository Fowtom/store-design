//
// Created by Tommy on 12/2/2016.
//

#ifndef STORE_DESIGN_STUBS_ENEMY_H
#define STORE_DESIGN_STUBS_ENEMY_H
#include <iostream>
#include <vector>
using namespace std;

class Enemy
{
    private:
        int enemyHealth;
        int enemyAttackValue;
        int enemyDefendValue;
        int enemyBalance;
        bool attackState;
        bool defendState;
        string enemyName;

    public:

        /**
         * Nondefault constructor for Enemy
         * @param enemyHealth Health of the enemy
         * @param enemyAttackValue Attack value of the enemy
         * @param enemyDefendValue Defend value of the enemy
         * @param enemyBalance Balance of the enemy
         * @param enemyName Name of the enemy
         * @return Nothing
         */
        Enemy(string enemyName, int enemyHealth, int enemyAttackValue, int enemyDefendValue, int enemyBalance);

        /**
         * Getter method for enemy's health
         * @return enemyHealth
         */
        int getEnemyHealth();

        /**
         * Getter method for enemy's attack value
         * @return enemyAttackVaule
         */
        int getEnemyAttackValue();

        /**
         * Getter method for enemy's defend value
         * @return enemyDefendValue
         */
        int getEnemyDefendValue();

        /**
         * Getter method for enemy's balance
         * @return enemyBalance
         */
        int getEnemyBalance();

        /**
         * Getter method for enemy's name
         * @return enemyName
         */
        string getEnemyName();

        /**
         * This method determines whether or not the enemy
         * is attacking
         * @param value 0 = false, 1 = true
         */
        void setenemyattackState(bool value);

        /**
         * This method determines whether or not the enemy
         * is defending
         * @param value 0 = false, 1 = true
         */
        void setenemydefendState(bool value);

        /**
         * Getter method for enemyAttackState
         * @return enemyAttackState
         */
        bool getenemyattackState();

        /**
         * Getter method for enemyDefendState
         * @return enemyAttackState
         */
        bool getenemydefendState();

        /**
         * Setter method that lowers enemy health
         * @param value refers to playerAttackValue
         */
        void removeenemyhealth(int value);
};


#endif //STORE_DESIGN_STUBS_ENEMY_H
