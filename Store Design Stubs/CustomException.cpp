//
// Created by Tommy on 12/2/2016.
//

#include "CustomException.h"

const char* CustomException::what() const throw()
{
    // Return string message
    return msg.c_str();
}