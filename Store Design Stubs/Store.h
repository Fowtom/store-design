//
// Created by Tommy on 11/3/2016.
//

#ifndef STORE_DESIGN_STUBS_STORE_H
#define STORE_DESIGN_STUBS_STORE_H
#include "vector"
#include "BaseSalableItem.h"
#include "BaseInventory.h"
#include "Player.h"

class Store : public BaseInventory
{
    protected:
        vector <BaseSalableItem*> Cart;
        int cartBalance = 0;
        Player* thePlayer = Player::instance();

    public:

        /**
         * Store destructor that gets called at the end of the
         * program. Removes all cart contents
         */
        ~Store();

        /**
         * Checks to see if the cart size is greater than zero so the
         * user can checkout. Used in most cases where the user needs
         * to view his/her cart.
         * @return True (1) if cart is not empty. Otherwise, return
         * false(0)
         */
        bool checkCartSize();

        /**
         * Iterates through size of cart and prints
         * out name of each item. Dependent on other methods
         * such as checkCartSize().
         */
        void printCart();

        /**
         * Iterates through size of cart and prints out all the
         * tokens for each item in the cart. Dependent on
         * other methods such as checkCartSize()
         */
        void printCartItemTokens();

        /**
         * Getter method for cart. This is used for saving and loading purposes
         * @return Cart
         */
        vector <BaseSalableItem*> get_cart();

        /**
         * Getter method for cart balance.
         * @return cartBalance
         */
        int get_cartBalance();

        /**
         * This method sets the balance of the cart
         * @param gold Amount
         * @return cartBalance = gold
         */
        void setcartBalance(int gold);
};


#endif //STORE_DESIGN_STUBS_STORE_H
