//
// Created by Tommy on 11/3/2016.
//

#ifndef STORE_DESIGN_STUBS_WEAPON_H
#define STORE_DESIGN_STUBS_WEAPON_H

#include "BaseSalableItem.h"

class Weapon : public BaseSalableItem
{
    public:

        /**
         * Nondefault constructor for weapon
         * @param name Name of the weapon item
         * @param type Type of the weapon item
         * @param description Description of the weapon item
         * @param cost Cost of the weapon item
         * @param stat Stat value of the weapon
         * @return Nothing
         */
        Weapon(string name, string type, string description, int cost, int stat);
};


#endif //STORE_DESIGN_STUBS_WEAPON_H
