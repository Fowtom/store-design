//
// Created by Tommy on 11/3/2016.
//

#include <iostream>
#include "BaseSalableItem.h"
using namespace std;

BaseSalableItem::BaseSalableItem()
{
    // Do nothing
}

BaseSalableItem::BaseSalableItem(string name, string type, string description, int cost, int stat)
{
    // Nondefault constructor that sets the values of each of the member variables
    this->name = name;
    this->type = type;
    this->description = description;
    this->cost = cost;
    this->stat = stat;
}

string BaseSalableItem::get_name()
{
    // Returns name of salable item.
    return name;
}

string BaseSalableItem::get_description()
{
    // Returns description of salable item.
    return description;
}

string BaseSalableItem::get_type()
{
    // Returns type of salable item
    return type;
}

int BaseSalableItem::get_cost()
{
    // Returns cost of salable item.
    return cost;
}

int BaseSalableItem::get_stat()
{
    // Returns stat of salable item
    return stat;
}