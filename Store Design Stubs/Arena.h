//
// Created by Tommy on 12/2/2016.
//

#ifndef STORE_DESIGN_STUBS_ARENA_H
#define STORE_DESIGN_STUBS_ARENA_H
#include <iostream>
#include <vector>
#include "Enemy.h"
#include "Player.h"

class Arena
{
    protected:
        vector <Enemy*> Enemies;
        Player* thePlayer = Player::instance();

    public:
        /**
         * This is the destructor for the arena. It will delete all created instances
         * of enemies and be executed at the end of the program
         */
        ~Arena();

        /**
         * This method checks the size of the Enemies vector to see whether or not it
         * is empty.
         * @return true or false
         */
        bool checkEnemiesSize();

        /**
         * This method adds enemies to the Enemies vector
         * @param enemy instance of enemy
         */
        void addEnemy(Enemy* enemy);

        /**
         * This method prints out all the tokens for each enemy. This includes name, health
         * attack, defense, and balance.
         */
        void printEnemyTokens();

        /**
         * Getter method for Enemies. Used for saving and loading purposes
         * @return Enemies
         */
        vector <Enemy*> get_Enemies();
};


#endif //STORE_DESIGN_STUBS_ARENA_H
