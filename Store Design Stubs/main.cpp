// Name: Thomas Fowler
// File Name: "Store Design Stubs"
// Date: 12-11-16
// Assignment Description: The main purpose of this whole project is to create a text based fantasy arena combat
//      game. The first part of this project involves creating a functional store that the user
//      can use to purchase better equipment (weapons, armor, and health). The user can then
//      use these items in the arena, which is the next part of the project. Each item will be
//      imported into the store's inventory from the weapon, armor, and health input files via
//      file/IO.
// PART II: In addition to the store, the user will now start off by entering a loading sequence where he/she can
// decide to loadfile a previous save file or start a new one. After this, GameLogic will be called and the user can
// decide if he/she wants to go to the store, go to the arena, equip an item, unequip an item, view stats, or exit.

#include <iostream>
#include "GameLogic.h"

using namespace std;

int main()
{
    // Create new pointer instance of GameLogic.
    GameLogic *g = new GameLogic;

    // Start up the game
    g->beginstartLogic();

    // Cleanup and return
    delete g;
    return 0;
}