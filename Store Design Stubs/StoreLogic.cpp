//
// Created by Tommy on 11/5/2016.
//

#include <iostream>
#include "StoreLogic.h"

void StoreLogic::promptStoreInput()
{
    // Print instructions for all the available options the user can work with.
    // These instructions include the key letters associated with each option. For
    // example: q = "Add to cart", w = "Sell item", e = "View cart and bag", r = "Remove
    // from cart", t = "Checkout", y = "Leave store".
    cout << "/----------------------------------/" << endl;
    cout << "/    Welcome to the Store Menu     /" << endl;
    cout << "/----------------------------------/" << endl;
    cout << "      Current balance: " << thePlayer->get_playerBalance() << endl;
    cout << "      Current cart balance: " << cartBalance << endl;
    cout << "/----------------------------------/" << endl;
    cout << "/ Press 'q' to 'Add to Cart'       /" << endl;
    cout << "/ Press 'w' to 'Sell Item'         /" << endl;
    cout << "/ Press 'e' to 'View Cart and Bag' /" << endl;
    cout << "/ Press 'r' to 'Remove from Cart'  /" << endl;
    cout << "/ Press 't' to 'Checkout'          /" << endl;
    cout << "/ Press 'y' to 'Leave Store'       /" << endl;
    cout << "/----------------------------------/" << endl << "" << endl;

    // Initialize user input validation
    bool validateInput = 0;

    // Validates user input
    while(validateInput == 0)
    {
        // Get user input
        cin >> userInput;

        // If user types out one of the key letters mentioned in the instructions
        if(userInput == "q" or userInput == "w" or userInput == "e" or userInput == "r" or userInput == "t" or userInput == "y" or userInput == "Tommy")
        {
            // Exit while loop
            validateInput = 1;
        }
        else
        {
            // Print feedback, letting user know his/her input is invalid
            cout << "Your input is invalid, please try again" << endl << "" << endl;
        }
    }

    // Each of the following conditionals calls the associated begin[task]Logic() method.

    // If user types "q"
    if(userInput == "q")
    {
        // Proceed with adding to cart
        beginaddtoCartLogic();
    }

    // If user types "w"
    if(userInput == "w")
    {
        // Proceed with selling an item
        beginsellLogic();
    }

    // If user types "e"
    if(userInput == "e")
    {
        // Proceed with viewing cart and bag
        beginviewCartandBagLogic();
    }

    // If user types "r"
    if(userInput == "r")
    {
        // Proceed with removing an item from cart
        beginremovefromCartLogic();
    }

    // If user types "t"
    if(userInput == "t")
    {
        // Proceed with checking out
        begincheckoutLogic();
    }

    // If user types "y"
    if(userInput == "y")
    {
        // Proceed with leaving the store
        beginleaveLogic();
    }

    // If user types "Tommy" **EASTER EGG**
    if(userInput == "Tommy")
    {
        // Prints feedback for the user
        cout << "That guy made this game. He's pretty cool!" << endl << "" << endl;

        // Return to store menu
        return promptStoreInput();
    }
}

void StoreLogic::beginaddtoCartLogic()
{
    // Checks size of inventory
    if(checkInventorySize() == 0)
    {
        // Gives feedback letting user know inventory is empty
        cout << "Your inventory is empty!" << endl;

        // Returns to store menu
        return promptStoreInput();
    }

    // Print out the inventory
    printInventoryItemTokens();

    // Prompts user for input, with an option to cancel
    cout << "Which item would you like to purchase? Type its name" << endl;
    cout << "Press 'c' to cancel" << endl << "" << endl;

    // Initialize input validator
    bool validateInput = 0;

    // Validates input
    while(validateInput == 0)
    {
        // Get user input
        cin >> userInput;

        // Iterate through size of inventory
        for (int i = 0; i < ItemList.size(); i++)
        {
            // Set the name of each item to string x
            string x = ItemList[i]->get_name();

            // If the user's input matches the item in inventory
            if (userInput.compare(x) == 0)
            {
                // Exit the while loop
                validateInput = 1;
            }
        }

        // If the user's input matches "c"
        if (userInput.compare("c") == 0)
        {
            // Return to store menu
            return promptStoreInput();
        }

        // If still in while loop
        if(validateInput == 0)
        {
            // Print feedback letting user know his/her input is invalid
            cout << "Your input was invalid, please try again" << endl;
        }
    }

    // Take item out of inventory and into cart
    // Iterate through size of inventory
    for (int i = 0 ; i < ItemList.size() ; i++)
    {
        // If the name of the item in the inventory matches the user input
        if(ItemList[i]->get_name() == userInput)
        {
            // Move that item to the back of the inventory
            swap(ItemList[i], ItemList.back());

            // Set the item in the back of the inventory to pointer variable "x" of type BaseSalableItem
            BaseSalableItem *x = ItemList.back();

            // Remove the last item in the inventory vector
            ItemList.pop_back();

            // Take the removed item from the inventory vector and put it in the cart vector
            Cart.push_back(x);

            // Print feedback, letting the user know the item has been added to his/her cart
            cout << "This item has been added to your cart!" << endl;
            cout << "The contents of your cart are: " << endl << "" << endl;

            // Print out items in cart
            printCart();

            cout << "" << endl;

            // Take the cost of the item that has been added to cart and add it to the cart balance
            cartBalance += x->get_cost();

            // Return to store menu
            return promptStoreInput();
        }
    }
}

void StoreLogic::beginsellLogic()
{
    // Checks to see if bag is empty
    if(thePlayer->checkBagSize() == 0)
    {
        // Return error
        cout << "Your bag is empty!" << endl;

        // Call store menu
        return promptStoreInput();
    }

    // Print "What would you like to sell?" with an option to cancel as well
    cout << "What would you like to sell?" << endl;
    cout << "Press 'c' to cancel" << endl << "" << endl;

    // Call printBag() from Player.
    thePlayer->printBag();

    cout << " " << endl;

    // Initialize user input validation
    bool validateInput = 0;

    // Validates user input
    while(validateInput == 0)
    {
        // Get user input
        cin >> userInput;

        // Iterate through the size of the bag
        for (int i = 0; i < thePlayer->get_bag().size(); i++)
        {
            // Set variable "x" to the name of the item in the bag
            string x = thePlayer->get_bag()[i]->get_name();

            // Checks to see if user typed in the name of the item
            if (userInput.compare(x) == 0)
            {
                // Exit the while loop
                validateInput = 1;
            }
        }

        // Checks to see if user typed "c" to cancel
        if (userInput.compare("c") == 0)
        {
            // Call store menu
            return promptStoreInput();
        }

        // If still in while loop
        if(validateInput == 0)
        {
            // Return error
            cout << "Your input was invalid" << endl;

            // Prints out the inventory
            thePlayer->printBag();
        }
    }

    // Take item out of bag
    // Iterate through size of bag
    for (int i = 0 ; i < thePlayer->get_bag().size() ; i++)
    {
        // If the name of the item in the bag matches the user's input
        if(thePlayer->get_bag()[i]->get_name().compare(userInput) == 0)
        {
            // Add the cost of the item to the player's balance
            thePlayer->addtoplayerBalance(thePlayer->get_bag()[i]->get_cost());

            // Print out user's balance
            cout << "Your current balance is: " << thePlayer->get_playerBalance() << endl;

            // Call removefrombag() to pop the item from the bag
            thePlayer->removefromBag(i);

            // Print feedback
            cout << "This item has been sold!" << endl;

            // Checks the size of the bag
            if(thePlayer->checkBagSize() == 0)
            {
                // Prints feedback
                cout << "Your bag is now empty!" << endl;
            }
            else
            {
                // Prints feedback
                cout << "The contents of your bag: " << endl << "" << endl;

                // Print out the contents in the user's bag
                thePlayer->printBag();

                cout << "" << endl;
            }
        }
    }

    // Return to store menu
    return promptStoreInput();
}

void StoreLogic::beginviewCartandBagLogic()
{
    // Call checkCartSize() to check if cart is empty
    if(!checkCartSize())
    {
        // Gives feedback letting the user know his/her cart is empty
        cout << "Your cart is empty!" << endl << "" << endl;
    }
    else
    {
        // Prints feedback
        cout << "Cart contents: " << endl << "" << endl;

        // Print out cart contents
        printCartItemTokens();

        cout << "" << endl;
    }

    // Call thePlayer->checkBagSize() to check if bag is empty
    if(!thePlayer->checkBagSize())
    {
        // Gives feedback
        cout << "Your bag is empty!" << endl << "" << endl;
    }
    else
    {
        // Gives feedback by printing out contents of bag
        cout << "Bag contents: " << endl << "" << endl;

        // Print out contents of bag
        thePlayer->printBag();

        cout << "" << endl;
    }

    // Call promptStoreInput()
    return promptStoreInput();
}

void StoreLogic::begincheckoutLogic()
{
    // Call checkCartSize() to check if cart is empty
    if(!checkCartSize())
    {
        // Prints feedback letting user know cart is empty
        cout << "Your cart is empty!" << endl << "" << endl;

        // Return to store menu
        return promptStoreInput();
    }

    // Prints feedback for cart contents
    cout << "Cart contents: " << endl << "" << endl;

    // Print out cart contents
    printCart();

    cout << "" << endl;

    // Prints cart balance
    cout << "Cart balance: " << cartBalance << endl;

    // Prints current player balance
    cout << "Your current balance: " << thePlayer->get_playerBalance() << endl << "" << endl;

    // Print feedback for removing any items from cart before checkout with three options
    cout << "Would you like to remove any items from your cart before checkout?" << endl;
    cout << "Type 'yes', 'no', or 'c' to cancel" << endl << "" << endl;

    // Initializes input validation
    bool validateInput = 0;

    // Validates input
    while(validateInput == 0)
    {
        // Gets user input
        cin >> userInput;

        // If user types "yes"
        if(userInput == "yes")
        {
            // Call beginremoveCartLogic().
            return beginremovefromCartLogic();
        }

        // If user types "no"
        else if(userInput == "no")
        {
            // Exit while loop
            validateInput = 1;
        }

        // If user types "c"
        else if(userInput == "c")
        {
            // Return to store menu
            return promptStoreInput();
        }
        else
        {
            // Prints feedback letting user know they mistyped
            cout << "Your input was invalid!" << endl << "" << endl;
        }
    }

    // Prints final feedback letting the user know how much he/she will be spending
    cout << "You will be spending " << cartBalance << " gold for purchasing the following items: " << endl << "" << endl;

    // Prints out the cart contents
    printCart();

    cout << "" << endl;

    // Prints out remaining player balance after checkout has been completed
    cout << "Your remaining balance will be: " << thePlayer->get_playerBalance() - cartBalance << endl;

    // Print feedback for finalizing checkout
    cout << "Are you sure you would like to checkout? Type 'yes' or 'no'" << endl;

    // Reinitialize input validator
    validateInput = 0;

    // Validates input
    while(validateInput == 0)
    {
        // Gets user input
        cin >> userInput;

        // If user types "yes"
        if(userInput == "yes")
        {
            // Exits while loop
            validateInput = 1;
        }

        // If user types "no"
        else if(userInput == "no")
        {
            // Return to main menu
            cout << "Bummer" << endl << "" << endl;
            return promptStoreInput();
        }
        else
        {
            // Print feedback letting user know he/she mistyped
            cout << "Your input is invalid, please try again." << endl;
        }
    }

    // If the user has enough gold
    if(cartBalance <= thePlayer->get_playerBalance())
    {
        // Take out the amount from the user's balance
        thePlayer->removefromplayerBalance(cartBalance);

        // Set new long variable as size of cart
        long defaultsize = Cart.size();

        // Iterates through size of the cart
        for (int i = 0 ; i < defaultsize ; i++)
        {
            // Creates a new pointer instance of BaseSalableItem and puts it in back of cart
            BaseSalableItem *x = Cart.back();

            // Removes last cart item from cart
            Cart.pop_back();

            // Adds the removed item to the bag
            thePlayer->addtoBag(x);

            // Diminishes cartBalance back to normal since all items are transferred to bag
            cartBalance -= x->get_cost();
        }

        // Prints feedback
        cout << "Thank you for shopping!" << endl;
        cout << "Your bag now contains: " << endl << "" << endl;

        // Prints bag contents
        thePlayer->printBag();

        cout << "" << endl;

    }
    else
    {
        // Prints feedback letting user know he/she does not have enough gold
        cout << "You do not have enough gold!" << endl;

        // Return to store menu
        return promptStoreInput();
    }

    // Return to store menu
    return promptStoreInput();
}

void StoreLogic::beginremovefromCartLogic()
{
    // Checks the size of the cart
    if(!checkCartSize())
    {
        // Prints feedback for if cart is empty
        cout << "Your cart is empty!" << endl;

        // Return to store menu
        return promptStoreInput();
    }

    // Print "What would you like to remove?" with an option to cancel by typing "c"
    cout << "What would you like to remove?" << endl;
    cout << "Press 'c' to cancel" << endl << "" << endl;

    // Print out cart contents
    printCart();

    cout << "" << endl;

    // Initialize input validator
    bool validateInput = 0;

    // Validates input
    while(validateInput == 0)
    {
        // Gets user input
        cin >> userInput;

        // Iterates through the size of the cart
        for (int i = 0; i < Cart.size(); i++)
        {
            // Create a variable "x" that is the name of the item in the cart
            string x = Cart[i]->get_name();

            // If the user types the name of the item in the cart
            if (userInput == x)
            {
                // Exit the while loop
                validateInput = 1;
            }
        }

        // If user types "c"
        if (userInput == "c")
        {
            // Return to store menu
            return promptStoreInput();
        }

        // If still in while loop
        if(validateInput == 0)
        {
            // Prints feedback letting the user know his/her input is invalid
            cout << "Your input was invalid, please try again" << endl << "" << endl;

            // Print cart contents
            printCart();

            cout << "" << endl;
        }
    }

    // For loop that iterates through size of the cart
    for (int i = 0 ; i < Cart.size() ; i++)
    {
        // If the name of the item in cart matches user input
        if(Cart[i]->get_name() == userInput)
        {
            // Move the item to the back of the cart
            swap(Cart[i], Cart.back());

            // Create a pointer variable of type BaseSalableItem that refers to the item
            BaseSalableItem *x = Cart.back();

            // Remove the item from the cart
            Cart.pop_back();

            // Put the item in the inventory
            ItemList.push_back(x);

            // Print feedback letting the user know the item has been removed from the cart
            cout << "This item has been removed from your cart!" << endl;

            // Checks if cart is empty
            if(checkCartSize() == 0)
            {
                // Prints feedback to let user know cart is empty
                cout << "Your cart is now empty!" << endl;
            }
            else
            {
                // Prints feedback to let user know contents of cart
                cout << "The contents of your cart: " << endl<< "" << endl;

                // Print cart contents
                printCart();

                cout << "" << endl;
            }

            // Update cartBalance since an item has been removed
            cartBalance -= x->get_cost();

            // Prints current cart balance
            cout << "Your current cart balance is: " << cartBalance << endl;

            // Return to store menu
            return promptStoreInput();
        }
    }

    // Return to store menu
    return promptStoreInput();
}


void StoreLogic::beginleaveLogic()
{
    // Prints feedback that gives the user the option to leave the store
    cout << "Are you sure you would like to leave the store? Type 'yes' to continue or any key to cancel" << endl << "" << endl;

    // Initializes input validator
    bool validateInput = 0;

    // Validates input
    while(validateInput == 0)
    {
        // Gets user input
        cin >> userInput;

        // If the user types "yes"
        if(userInput.compare("yes") == 0)
        {
            // Prints "Thank you for using the store!"
            cout << "Thank you for using the store!" << endl << "" << endl;

            // Exits the while loop
            validateInput = 1;
        }
        else
        {
            // Return to the main menu
            return promptStoreInput();
        }
    }
}
