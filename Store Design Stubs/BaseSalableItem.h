//
// Created by Tommy on 11/3/2016.
//

#ifndef STORE_DESIGN_STUBS_SALABLEITEM_H
#define STORE_DESIGN_STUBS_SALABLEITEM_H
#include <string>

using namespace std;

class BaseSalableItem
{
    protected:
        string name = "";
        string type = "";
        string description = "";
        int cost = 0;
        int stat = 0;

    public:

        /**
         * Default constructor for BaseSalableItem
         * @return Nothing
         */
        BaseSalableItem();

        /**
         * Nondefault constructor
         * @param name sets name of salable item
         * @param type sets type of salable item
         * @param cost sets cost of salable item
         * @param description sets description of salable item
         * @param stat sets the stat of salable item
         * @return the values for each of the parameters
         */
        BaseSalableItem(string name, string type, string description, int cost, int stat);

        /**
         * Getter method for protected member variable name
         * @return name
         */
        string get_name();


        /**
         * Getter method for protected member variable description
         * @return description
         */
        string get_description();

        /**
         * Getter method for protected member variable type
         * @return type
         */
        string get_type();

        /**
         * Getter method for protected member variable cost
         * @return cost
         */
        int get_cost();

        /**
         * Getter method for protected member variable stat
         * @return stat
         */
        int get_stat();

        /**
         * This method creates instances of all the items. It is used in the SalableItemFactory class, and declared
         * here in the base class.
         * @param name Name of the item
         * @param type Type of the item
         * @param description Description of the item
         * @param cost Cost of the item
         * @param stat Stat of the item
         * @return New instances of Weapon, armor, and health
         */
        static BaseSalableItem* create(string name, string type, string description, int cost, int stat);
};


#endif //STORE_DESIGN_STUBS_SALABLEITEM_H
