//
// Created by Tommy on 12/2/2016.
//

#include "Game.h"
#include <fstream>
#include <sstream>

using namespace std;

int Game::loaddefaultfile()
{
    // Create pointer instance of item.
    BaseSalableItem *item;

    // Create pointer instance of enemy
    Enemy *enemy;

    // Open armor file.
    ifstream armor_input;

    try
    {
        // Open armor input file
        armor_input.open("./Armor.txt", ios::in);

        // If the armor input file cannot be opened
        if(!armor_input)
        {
            // Close the file
            armor_input.close();

            // Throw exception indicating the file cannot be opened
            throw invalid_argument("Could not open armor input file");
        }
    }
    catch(invalid_argument x)
    {
        // Print exception message
        cout << x.what() << endl;
        return -1;
    }

    // Read and parse through armor file
    // Create token strings
    string armor_token1, armor_token2, armor_token3, armor_token4, armor_token5;

    // String represents line in the armor input file
    string armor_line;

    // While loop that parses through armor input file
    while (getline(armor_input, armor_line))
    {
        // Create a string stream from the line and parse its tokens using a | delimeter
        istringstream lineStream(armor_line);
        getline(lineStream, armor_token1, '|');
        getline(lineStream, armor_token2, '|');
        getline(lineStream, armor_token3, '|');
        getline(lineStream, armor_token4, '|');
        getline(lineStream, armor_token5, '|');

        try
        {
            // Create a new instance of armor item for each line in the input file
            item = BaseSalableItem::create(armor_token1, armor_token2, armor_token3, stoi(armor_token4), stoi(armor_token5));

            // If the item is not of type "Armor"
            if(item == NULL)
            {
                // Throw exception indicating the item type does not match
                throw invalid_argument("The type for one or more of your items is not 'Armor'");
            }

            // Put armor item(s) in inventory.
            theStoreLogic->addSalableItem(item);

        }
        catch(invalid_argument x)
        {
            // Print exception message
            cout << x.what() << endl;
            return -1;
        }
    }

    // Close armor input file
    armor_input.close();

    // Open health file.
    ifstream health_input;

    try
    {
        // Open health input file
        health_input.open("./Health.txt", ios::in);

        // If the health input file cannot be opened
        if(!health_input)
        {
            // Close the file
            health_input.close();

            // Throw exception indicating the file cannot be opened
            throw invalid_argument("Could not open health input file");
        }
    }
    catch(invalid_argument x)
    {
        // Print exception message
        cout << x.what() << endl;
        return -1;
    }

    // Read and parse through health file.
    // Create token strings
    string health_token1, health_token2, health_token3, health_token4, health_token5;

    // String represents line in the health input file
    string health_line;

    // While loop that parses through health input file
    while (getline(health_input, health_line))
    {
        // Create a string stream from the line and parse its tokens using a | delimeter
        istringstream lineStream(health_line);
        getline(lineStream, health_token1, '|');
        getline(lineStream, health_token2, '|');
        getline(lineStream, health_token3, '|');
        getline(lineStream, health_token4, '|');
        getline(lineStream, health_token5, '|');

        try
        {
            // Create a new instance of health item for each line in the input file
            item = BaseSalableItem::create(health_token1, health_token2, health_token3, stoi(health_token4), stoi(health_token5));

            // If the item is not of type "Health"
            if(item == NULL)
            {
                // Throw exception indicating the item type does not match
                throw invalid_argument("The type for one or more of your items is not 'Health'");
            }

            // Put armor item(s) in inventory.
            theStoreLogic->addSalableItem(item);

        }
        catch(invalid_argument x)
        {
            // Print exception message
            cout << x.what() << endl;
            return -1;
        }
    }

    // Close health input file
    health_input.close();

    // Open weapon file.
    ifstream weapon_input;

    try
    {
        // Open weapon input file
        weapon_input.open("./Weapon.txt", ios::in);

        // If the weapon input file cannot be opened
        if(!weapon_input)
        {
            // Close the file
            weapon_input.close();

            // Throw exception indicating the file cannot be opened
            throw invalid_argument("Could not open weapon input file");
        }
    }
    catch(invalid_argument x)
    {
        // Print exception message
        cout << x.what() << endl;
        return -1;
    }

    // Read and parse through weapon file.
    // Create token strings
    string weapon_token1, weapon_token2, weapon_token3, weapon_token4, weapon_token5;

    // String represents line in the weapon input file
    string weapon_line;

    // While loop that parses through weapon input file
    while (getline(weapon_input, weapon_line))
    {
        // Create a string stream from the line and parse its tokens using a | delimeter
        istringstream lineStream(weapon_line);
        getline(lineStream, weapon_token1, '|');
        getline(lineStream, weapon_token2, '|');
        getline(lineStream, weapon_token3, '|');
        getline(lineStream, weapon_token4, '|');
        getline(lineStream, weapon_token5, '|');

        try
        {
            // Create a new instance of weapon item for each line in the input file
            item = BaseSalableItem::create(weapon_token1, weapon_token2, weapon_token3, stoi(weapon_token4), stoi(weapon_token5));

            // If the item is not of type "Weapon"
            if(item == NULL)
            {
                // Throw exception indicating the item type does not match
                throw invalid_argument("The type for one or more of your items is not 'Weapon'");
            }

            // Put armor item(s) in inventory.
            theStoreLogic->addSalableItem(item);

        }
        catch(invalid_argument x)
        {
            // Print exception message
            cout << x.what() << endl;
            return -1;
        }
    }

    // Close weapon input file
    weapon_input.close();


    // Open enemy file.
    ifstream enemy_input;

    try
    {
        // Open enemy input file
        enemy_input.open("./Enemy.txt", ios::in);

        // If the enemy input file cannot be opened
        if(!enemy_input)
        {
            // Close the file
            enemy_input.close();

            // Throw exception indicating the file cannot be opened
            throw invalid_argument("Could not open enemy input file");
        }
    }
    catch(invalid_argument x)
    {
        // Print exception message
        cout << x.what() << endl;
        return -1;
    }

    // Read and parse through enemy file.
    // Create token strings
    string enemy_token1, enemy_token2, enemy_token3, enemy_token4, enemy_token5;

    // String represents line in the enemy input file
    string enemy_line;

    // While loop that parses through enemy input file
    while (getline(enemy_input, enemy_line))
    {
        // Create a string stream from the line and parse its tokens using a | delimeter
        istringstream lineStream(enemy_line);
        getline(lineStream, enemy_token1, '|');
        getline(lineStream, enemy_token2, '|');
        getline(lineStream, enemy_token3, '|');
        getline(lineStream, enemy_token4, '|');
        getline(lineStream, enemy_token5, '|');

        // Create a new instance of enemy for each line in the input file
        enemy = new Enemy(enemy_token1, stoi(enemy_token2), stoi(enemy_token3), stoi(enemy_token4), stoi(enemy_token5));

        // Put enemies in Enemies vector
        theArenaLogic->addEnemy(enemy);
    }

    // Close enemy input file
    enemy_input.close();

    return 0;
}

void Game::save()
{
    // Create output instance of save file
    ofstream saveOutputFile;

    try
    {
        // Open save output file
        saveOutputFile.open("./Save.txt", ios::out);

        // If the save file cannot be found
        if(!saveOutputFile)
        {
            // Close the file
            saveOutputFile.close();

            // Throw exception indicating the file cannot be opened
            throw invalid_argument("Could not open save file");
        }
    }
    catch(invalid_argument x)
    {
        // Print exception message
        cout << x.what() << endl;
        return;
    }

    // Print out player information in save file, separated by pipe delimiter
    saveOutputFile << thePlayer->get_name() << "|" << thePlayer->get_playerhealth() << "|" << thePlayer->get_playerattackValue() << "|";
    saveOutputFile << thePlayer->get_playerdefendValue() << "|" << thePlayer->get_playerBalance() << "|~" << "\r\n";

    // Iterate through size of player's bag
    for(int i = 0 ; i < thePlayer->get_bag().size() ; i++)
    {
        // Print out player bag information, separated by pipe delimiter
        saveOutputFile << thePlayer->get_bag()[i]->get_name() << "|" << thePlayer->get_bag()[i]->get_type() << "|";
        saveOutputFile << thePlayer->get_bag()[i]->get_description() << "|" << thePlayer->get_bag()[i]->get_cost() << "|";
        saveOutputFile << thePlayer->get_bag()[i]->get_stat();

        // If the whole bag has been iterated
        if(i < thePlayer->get_bag().size() - 1)
        {
            // Print out a special character to indicate the vector is finished
            saveOutputFile << "|~|";
        }
    }

    // Print out delimiter
    saveOutputFile << "|" << "\r\n";

    // Iterate through size of player's equippedItems vector
    for(int i = 0 ; i < thePlayer->get_equippedItems().size() ; i++)
    {
        // Print out equippedItems vector information, separated by pipe delimiter
        saveOutputFile << thePlayer->get_equippedItems()[i]->get_name() << "|" << thePlayer->get_equippedItems()[i]->get_type() << "|";
        saveOutputFile << thePlayer->get_equippedItems()[i]->get_description() << "|" << thePlayer->get_equippedItems()[i]->get_cost() << "|";
        saveOutputFile << thePlayer->get_equippedItems()[i]->get_stat();

        // If the whole equippedItems vector has been iterated
        if(i < thePlayer->get_equippedItems().size() - 1)
        {
            // Print out special character to indicate vector is finished
            saveOutputFile << "|~|";
        }
    }

    // Print out delimiter
    saveOutputFile << "|" << "\r\n";

    // Iterate through size of player's cart vector
    for(int i = 0 ; i < theStoreLogic->get_cart().size() ; i++)
    {
        // Print out cart vector information, separated by pipe delimiter
        saveOutputFile << theStoreLogic->get_cart()[i]->get_name() << "|" << theStoreLogic->get_cart()[i]->get_type() << "|";
        saveOutputFile << theStoreLogic->get_cart()[i]->get_description() << "|" << theStoreLogic->get_cart()[i]->get_cost() << "|";
        saveOutputFile << theStoreLogic->get_cart()[i]->get_stat();

        // If the whole cart vector has been iterated
        if(i < theStoreLogic->get_cart().size() - 1)
        {
            // Print out special character to indicate vector is finished
            saveOutputFile << "|~|";
        }
    }

    // Print out delimiter
    saveOutputFile << "|" << "\r\n";

    // Print out cart balance
    saveOutputFile << theStoreLogic->get_cartBalance() << "\r\n";

    // Iterate through size of Inventory vector
    for(int i = 0 ; i < theStoreLogic->get_ItemList().size() ; i++)
    {
        // Print out inventory vector information, separated by pipe delimiter
        saveOutputFile << theStoreLogic->get_ItemList()[i]->get_name() << "|" << theStoreLogic->get_ItemList()[i]->get_type() << "|";
        saveOutputFile << theStoreLogic->get_ItemList()[i]->get_description() << "|" << theStoreLogic->get_ItemList()[i]->get_cost() << "|";
        saveOutputFile << theStoreLogic->get_ItemList()[i]->get_stat();

        // If the whole Inventory has been iterated
        if(i < theStoreLogic->get_ItemList().size() - 1)
        {
            // Print out special character to indicate vector is finished
            saveOutputFile << "|~|";
        }
    }

    // Print out delimiter
    saveOutputFile << "|" << "\r\n";

    // Iterate through size of Enemies vector
    for(int i = 0 ; i < theArenaLogic->get_Enemies().size() ; i++)
    {
        // Print out Enemies vector information, separated by pipe delimiter
        saveOutputFile << theArenaLogic->get_Enemies()[i]->getEnemyName() << "|" << theArenaLogic->get_Enemies()[i]->getEnemyHealth() << "|";
        saveOutputFile << theArenaLogic->get_Enemies()[i]->getEnemyAttackValue() << "|" << theArenaLogic->get_Enemies()[i]->getEnemyDefendValue() << "|";
        saveOutputFile << theArenaLogic->get_Enemies()[i]->getEnemyBalance();

        // If the whole Enemies vector has been iterated
        if(i < theArenaLogic->get_Enemies().size() - 1)
        {
            // Print out special character to indicate vector is finished
            saveOutputFile << "|~|";
        }
    }

    // Print out delimiter
    saveOutputFile << "|" << "\r\n";

    // Close the save output file
    saveOutputFile.close();
}

int Game::loadfile()
{

    // Create ifstream variable loadOutputFile
    ifstream loadOutputFile;

    try
    {
        // Open save file
        loadOutputFile.open("./Save.txt", ios::in);

        // If the save file cannot be found
        if(!loadOutputFile)
        {
            // Close the file
            loadOutputFile.close();

            // Throw exception indicating the file cannot be opened
            throw invalid_argument("Could not open save file");
        }
    }
    catch(invalid_argument x)
    {
        // Print exception message
        cout << x.what() << endl;
        return -1;
    }

    // Iterator that increases as save file is read
    int a = 0;

    // These are the tokens that will be checked when the save file is parsed
    string token1, token2, token3, token4, token5, token6;

    // String represents line in the save input file
    string output_line;

    // Create pointer instance of item.
    BaseSalableItem *item;

    // Create pointer instance of enemy
    Enemy *enemy;

    // While loop that parses through save input file
    while (getline(loadOutputFile, output_line))
    {
        // String stream for each line of output
        istringstream lineStream(output_line);

        // Player information
        if(a == 0)
        {
            // Create a string stream from the line and parse its tokens using a | delimeter
            getline(lineStream, token1, '|');
            getline(lineStream, token2, '|');
            getline(lineStream, token3, '|');
            getline(lineStream, token4, '|');
            getline(lineStream, token5, '|');
            getline(lineStream, token6, '|');

            thePlayer->setplayername(token1);
            thePlayer->setplayerhealth(stoi(token2));
            thePlayer->setplayerattackValue(stoi(token3));
            thePlayer->setplayerdefendValue(stoi(token4));
            thePlayer->setplayerBalance(stoi(token5));
        }

        // Bag information
        while(a == 1)
        {
            // Create a string stream from the line and parse its tokens using a | delimeter
            getline(lineStream, token1, '|');
            getline(lineStream, token2, '|');
            getline(lineStream, token3, '|');
            getline(lineStream, token4, '|');
            getline(lineStream, token5, '|');
            getline(lineStream, token6, '|');

            // Create a new instance of item for each line in the input file
            item = BaseSalableItem::create(token1, token2, token3, stoi(token4), stoi(token5));
            thePlayer->addtoBag(item);

            // Continue on with while loop
            a += 1;
        }

        // equippedItems information
        while(a == 3)
        {
            // Create a string stream from the line and parse its tokens using a | delimeter
            getline(lineStream, token1, '|');
            getline(lineStream, token2, '|');
            getline(lineStream, token3, '|');
            getline(lineStream, token4, '|');
            getline(lineStream, token5, '|');
            getline(lineStream, token6, '|');

            if(token6 == "~")
            {
                // Create a new instance of item for each line in the input file
                item = BaseSalableItem::create(token1, token2, token3, stoi(token4), stoi(token5));
                thePlayer->addtoequippedItems(item);

                // Continue on with while loop
            }
            else
            {
                // Exit while loop, continue to next vector
                a += 1;
            }
        }

        // Cart information
        while(a == 5)
        {
            // Create a string stream from the line and parse its tokens using a | delimeter
            getline(lineStream, token1, '|');
            getline(lineStream, token2, '|');
            getline(lineStream, token3, '|');
            getline(lineStream, token4, '|');
            getline(lineStream, token5, '|');
            getline(lineStream, token6, '|');

            if(token6 == "~")
            {
                // Create a new instance of item for each line in the input file
                item = BaseSalableItem::create(token1, token2, token3, stoi(token4), stoi(token5));
                thePlayer->addtobag(item);

                // Continue on with while loop
            }
            else
            {
                // Exit while loop, continue to next vector
                a += 1;
            }
        }

        // Cart balance information
        if(a == 7)
        {
            getline(lineStream, token1, '|');
            theStoreLogic->setcartBalance(stoi(token1));
        }

        // ItemList information
        while(a == 8)
        {
            // Create a string stream from the line and parse its tokens using a | delimeter
            getline(lineStream, token1, '|');
            getline(lineStream, token2, '|');
            getline(lineStream, token3, '|');
            getline(lineStream, token4, '|');
            getline(lineStream, token5, '|');
            getline(lineStream, token6, '|');

            if(token6 == "~")
            {
                // Create a new instance of item for each line in the input file
                item = BaseSalableItem::create(token1, token2, token3, stoi(token4), stoi(token5));
                theStoreLogic->get_ItemList().push_back(item);

                // Continue on with while loop
            }
            else
            {
                // Exit while loop, continue to next vector
                a += 1;
            }
        }

        // Enemies Information
        while(a == 10)
        {
            // Create a string stream from the line and parse its tokens using a | delimeter
            getline(lineStream, token1, '|');
            getline(lineStream, token2, '|');
            getline(lineStream, token3, '|');
            getline(lineStream, token4, '|');
            getline(lineStream, token5, '|');
            getline(lineStream, token6, '|');

            if(token6 == "~")
            {
                // Create a new instance of item for each line in the input file
                enemy = new Enemy(token1, stoi(token2), stoi(token3), stoi(token4), stoi(token5));

                // Put enemies in Enemies vector
                theArenaLogic->addEnemy(enemy);

                // Continue on with while loop
            }
            else
            {
                // Exit while loop, continue to next vector
                a += 1;
            }
        }

        // Iterate to next line
        a += 1;
    }

    loadOutputFile.close();

    return 0;
}

Game::~Game()
{
    // Delete all instances of StoreLogic, ArenaLogic, and Player
    delete thePlayer;
    delete theArenaLogic;
    delete theStoreLogic;
}