Thomas Fowler
CST-210, MWF155A
Mark Reha
13 November 2016

FANTASY FIGHTING GAME PART I: THE STORE

Main Purpose
************
The main purpose of this whole project is to create a text based fantasy arena combat
game. The first part of this project involves creating a functional store that the user
can use to purchase better equipment (weapons, armor, and health). The user can then
use these items in the arena, which is the next part of the project. Each item will be
imported into the store's inventory from the weapon, armor, and health input files via
file/IO.

Game Description
****************
This game is based off of the Shaman hero from the popular online card game, Hearthstone.
The player will start off with a weapon, some armor and food to help him/her fight off
enemies in the arena. Enemies drop gold which can be spent in the store to upgrade equipment.
The items that are in the inventory of the store will be imported from their respected
input files.

Functionality
*************
This program meets the main purpose in a variety of ways. The user receives direct
feedback so he/she will always know what is going on. All user inputs are text-based,
and it allows the user to have full control of the player within the context of the store.

Approach + Implementation
*************************
The store consists of six main actions, and each action have its own key that is
inputted by the user:

 Add to Cart (key: q)
 Sell Item (key: w)
 View Cart and Bag (key: e)
 Remove Item from Cart (key: r)
 Checkout (key: t)
 Leave Store (key: y)

Game Rules
**********

1) Start off by fighting in the arena! Press the space bar to attack enemies.
2) Whenever you defeat an enemy, you will gain some gold that can be spent at the store.
3) Type in "Store" to access the store and buy more weapons, food, and armor.
4) In the store, there are six possible options: add to cart, sell, view cart,
   checkout, remove item from cart, and leave store.
5) The store can be used as a means of upgrading your equipment to make fighting enemies
   easier!
6) The goal is to get the best equipment possible so enemy fights will be effortless!

How to open and run the program
*******************************

1) Start off by making sure every file in this submission is in the same directory. These
   files include the report template, the readme, the flowchart, the UML class diagram, 
   the input files folder, the screenshots folder, and a folder with the actual program
   itself.
2) Within the actual program folder is a file called "main.cpp". Double click on this file
   and open it in your default C++ IDE (CLion).
3) Wait a few moments for the build of the program to finish loading.
4) Once it has finished building, go to lines 32, 69, and 106 and change the directories of
   to Armor.txt, Health.txt, and Weapon.txt, respectively. These input files are located in
   the input files folder in the original submission.
5) There are three ways to run the program:

   a) Click the green play button in the top right-hand corner.
   b) Use the hotkey Shift+F10.
   c) At the top menu bar, select Run -> Run 'Store Design Stubs'

6) When the program runs, the logic of main will execute.

Software Required
*****************
Clion

Input Data Files
****************
Armor.txt
Health.txt
Weapon.txt

Game Logic
**********

- Main -
The logic starts off in main(). Instances of StoreLogic and BaseSalableItem are made, and
all of the input files (Armor.txt, Health.txt, and Weapon.txt) are opened, read, and parsed.
This all occurs within three separate while loops for each input file, and each of the tokens
are compiled and a new item is created and added to the inventory, ItemList. Once this is
completed, promptInput(), the main driver method within the class StoreLogic, is called.

- Store Logic -
The StoreLogic class comes into play after the method promptInput() has been called. It starts
off by printing out "Welcome to the Store Menu!", as well as printing out several options that
the user can choose from for specific actions. Specifically, these actions were mentioned
previously in the "Approach and Implementation" section:

 Add to Cart (key: q)
 Sell Item (key: w)
 View Cart and Bag (key: e)
 Remove Item from Cart (key: r)
 Checkout (key: t)
 Leave Store (key: y)

After the user has inputted his/her option, promptInput() will call the respective 
begin[task]Logic() method associated with that task.

- Add to Cart -
beginaddtocartLogic() begins by printing out "Which item would you like to purchase? Type its name." 
All of the items in the inventory are then printed out, as well as the details for each item. The user
will then be prompted to type the name of the item he/she wishes to add to the cart, and will
receive feedback if the input did not match up. If the input does match with one of the items
in the inventory, the item will be popped out of the inventory and pushed into the cart. The
cart and cart balance will be printed out and the user will receive feedback. After this,
promptInput() will be called.

- Sell Item -
Begins by checking if the bag size is empty, because if it is, it will print out "Your bag
is empty!" and recall promptInput(). If there is anything in the bag, beginSellLogic() will
continue. It prints out the bag contents, and the user will input his/her desired item to sell.
The input will be validated, and if it is valid, the item's cost will be added to the player's
balance and the item will be removed from the player's bag. After this, promptInput() will
be called.

- View Cart and Bag -
This method, beginviewCartandBagLogic(), prints out the contents of the cart and bag. If either
is empty, it will print out "Your bag/cart is empty!", and after this, promptInput() will be
called.

- Checkout -
This method, beginCheckoutLogic(), checks to see if the cart size is greater than 0 because
if it is not, it will print out "Your cart is empty!" and recall promptInput(). After this,
the cart contents will be printed out. The user will be prompted if the user would like to
remove any items from his/her cart before checking out. If the user types "yes",
beginremovefromCartLogic() will be called. Otherwise, the input will be validated and it will
print out how much the user will be spending, what items he/she will be purchasing, and what
his/her remaining balance will be. If the user confirms by typing "yes" it will check to see
if the balance is >= the cart balance. If no, then it will print out "You do not have enough
gold!" and recall promptInput(). If yes, then it will print out "Thank you for using the
store!" and it will subtract the cart balance from the player balance, as well as pop the
items from the cart and push them to the player's bag, and promptInput() will be called.

- Remove from Cart -
This method, beginremovefromCartLogic(), starts off by checking the cart size to make sure
the cart is not empty before proceeding. If it is, it will call promptInput(). The cart contents
will be printed, and it will print out "What would you like to remove?". The user will input
his/her desired item, and if the input matches with one of the items in the cart, it will 
print out "Your input was invalid" and reprint out the cart. If the input does match, it will
pop the selected item out of the cart and push it into the inventory. The user will receive
feedback letting them know that it was successful, and if the cart is empty as a result, it 
will print "Your cart is now empty!" and call promptInput(). Otherwise, it will print out
the contents of the cart and call promptInput().

- Leave Store -
This method, beginleaveLogic(), starts off by printing out "Are you sure you would like to
leave the store? Type 'yes' to continue or any key to cancel." If the user types "yes", it
will print out "Thank you for using the store!" and exit the code. Otherwise, promptInput()
will be called.

Design Features
***************
The shopping cart itself is a vector of type <BaseSalableItem*> that automatically expands
and shrinks as items are taken or removed from the cart. The inventory, named ItemList, 
is also a vector of type <BaseSalableItem*> that contains the list of items in the store.
The last vector is the users bag, and it is also of type <BaseSalableItem*>. Allowing the
vectors to be of this type optimizes speed, memory, and efficiency of the program.

There is a total of eight classes:
 Store
 StoreLogic
 BaseInventory
 Player
 BaseSalableItem
 Weapon
 Armor
 Health

Each class has its own private / protected member variables and public methods. Inheritance
is exercised in this project by the "Has-A" and "Is-A" relationships between the classes.
For example, the classes Weapon, Armor, and Health all have an Is-A relationship with the
BaseSalableItem class.

List of Weapons
***************
 Spirit Claws (default weapon)
	- Type: Weapon
	- Cost: 100 gold
	- Description: "A beginner's choice!"
	- Attack Stat: 5

 Hammer of Twilight
	- Type: Weapon
	- Cost: 750 gold
	- Description: "Packs a big punch!"
	- Attack Stat: 25

 Doomhammer
	- Type: Weapon
	- Cost: 1000 gold
	- Description: "The best weapon in the game!"
	- Attack Stat: 40

Each of the weapons are of type BaseSalableItem, and they are created in main() in the while
loop that parses through the input file for weapon, Weapon.txt. 

Challenges
**********
I think the hardest part of this project was translating my design into the syntax of the
code itself. Managing the vector(s) and optimizing speed and memory was among one
of the many challenges in this project. Additionally, this project required a lot
of time, and I invested a lot more time in this project compared to past projects.

Todo
****

I have completed everything that I wanted to do in this program.

Known Bugs
**********

This program has no known bugs.


