Thomas Fowler
CST-210, MWF155A
Mark Reha
4 December 2016

Game Logic
**********

The game starts off by creating instances of StoreLogic, ArenaLogic, and Player. After this,
it fills the store with items, and fills the arena with enemies. The user will be prompted
to either start a new game, or load a previous save file. If they decide to load, it will
check to see if a save file has been made previously. If yes, it will load up the save and
start up the driver. Otherwise, the user will be notified that they don't have a save file
and reprompt to load or start a new game.

The main driver will say: "Would you like to view your stats, equip, unequip, go to the
arena, store, or exit?". The user will type a key letter associated with each of those options
and be redirected depending on what they input. If they decide to go to the store, it will
follow the logic of the store flowchart, save progress, and redirect back to the main
driver. Similarly, if they decide to go to the arena, it will follow the logic of the arena
flowchart, and check to see if the user has beat the game. If they have not, it will save
and redirect to the main driver. If they have, it will end the game and exit the code. If
the user decides to exit, it will save progress and exit the code.

If the user wants to equip an item, it will start off by checking to see if the player's bag
is empty. If it is, it will print "Your bag is empty!" and redirect the user back to the main
driver. If it is not, it will check to see if any of the items in the bag are weapons or
armor. If none of the items are weapon or armor, it will print "You don't have any items
in your bag that can be equipped", and redirect the user back to the main driver. If the user
does have any weapons or armor in their bag, it will print out the contents of the bag and
it will print out "What would you like to equip?". The user will input their desired item,
and it will check to see if the input matches any item in the bag. If it does not, it will
print "Input was invalid, please try again" and reprompt the user. If it does, it will check
to see if the inputted item is a weapon or armor item. If it is not, it will print "Input was
not a weapon or armor, please try again" and reprompt the user. If it is, it will pop the item
out of the bag and equip the item, as well as add the stat to player stats and print out
"You have equipped your item!" and return to the main game driver.

If the user wants to unequip an item, it will start off by checking to see if the player has
any items equipped. If they do not, it will print "You haven't equipped any items!" and 
redirect the user back to the main driver. If they have, it will print out list of equipped
items and print out "What would you like to unequip?". The user will input their desired item,
and if the input does not match any of the equipped items, it will print out "Input was
invalid, please try again" and reprompt the user. If it does match, then it will remove the
equipped item and push it into the bag, subtract the stat from the player stats, print
out "You have unequipped your item!", and return to the main game driver.

If the user wants to view their stats, it will start off by checking to see if the player has
any items equipped. If they do not, it will print "You're defenseless! Equip a weapon and some
armor to defend yourself" and redirect the user back to the main driver. If they do, it will
print out "You have equipped the following items: " and print out the list of equipped items.
It will then print out "Your current stats are: " and then print out the player's stats, and
then return to the main game driver.

Store Logic
***********

- Main -
The logic starts off in main(). Instances of StoreLogic and BaseSalableItem are made, and
all of the input files (Armor.txt, Health.txt, and Weapon.txt) are opened, read, and parsed.
This all occurs within three separate while loops for each input file, and each of the tokens
are compiled and a new item is created and added to the inventory, ItemList. Once this is
completed, promptInput(), the main driver method within the class StoreLogic, is called.

- Store Logic -
The StoreLogic class comes into play after the method promptInput() has been called. It starts
off by printing out "Welcome to the Store Menu!", as well as printing out several options that
the user can choose from for specific actions. Specifically, these actions were mentioned
previously in the "Approach and Implementation" section:

� Add to Cart (key: �q�)
� Sell Item (key: �w�)
� View Cart and Bag (key: �e�)
� Remove Item from Cart (key: �r�)
� Checkout (key: �t�)
� Leave Store (key: �y�)

After the user has inputted his/her option, promptInput() will call the respective 
begin[task]Logic() method associated with that task.

- Add to Cart -
beginaddtocartLogic() begins by printing out "Which item would you like to purchase? Type its name." 
All of the items in the inventory are then printed out, as well as the details for each item. The user
will then be prompted to type the name of the item he/she wishes to add to the cart, and will
receive feedback if the input did not match up. If the input does match with one of the items
in the inventory, the item will be popped out of the inventory and pushed into the cart. The
cart and cart balance will be printed out and the user will receive feedback. After this,
promptInput() will be called.

- Sell Item -
Begins by checking if the bag size is empty, because if it is, it will print out "Your bag
is empty!" and recall promptInput(). If there is anything in the bag, beginSellLogic() will
continue. It prints out the bag contents, and the user will input his/her desired item to sell.
The input will be validated, and if it is valid, the item's cost will be added to the player's
balance and the item will be removed from the player's bag. After this, promptInput() will
be called.

- View Cart and Bag -
This method, beginviewCartandBagLogic(), prints out the contents of the cart and bag. If either
is empty, it will print out "Your bag/cart is empty!", and after this, promptInput() will be
called.

- Checkout -
This method, beginCheckoutLogic(), checks to see if the cart size is greater than 0 because
if it is not, it will print out "Your cart is empty!" and recall promptInput(). After this,
the cart contents will be printed out. The user will be prompted if the user would like to
remove any items from his/her cart before checking out. If the user types "yes",
beginremovefromCartLogic() will be called. Otherwise, the input will be validated and it will
print out how much the user will be spending, what items he/she will be purchasing, and what
his/her remaining balance will be. If the user confirms by typing "yes" it will check to see
if the balance is >= the cart balance. If no, then it will print out "You do not have enough
gold!" and recall promptInput(). If yes, then it will print out "Thank you for using the
store!" and it will subtract the cart balance from the player balance, as well as pop the
items from the cart and push them to the player's bag, and promptInput() will be called.

- Remove from Cart -
This method, beginremovefromCartLogic(), starts off by checking the cart size to make sure
the cart is not empty before proceeding. If it is, it will call promptInput(). The cart contents
will be printed, and it will print out "What would you like to remove?". The user will input
his/her desired item, and if the input matches with one of the items in the cart, it will 
print out "Your input was invalid" and reprint out the cart. If the input does match, it will
pop the selected item out of the cart and push it into the inventory. The user will receive
feedback letting them know that it was successful, and if the cart is empty as a result, it 
will print "Your cart is now empty!" and call promptInput(). Otherwise, it will print out
the contents of the cart and call promptInput().

- Leave Store -
This method, beginleaveLogic(), starts off by printing out "Are you sure you would like to
leave the store? Type 'yes' to continue or any key to cancel." If the user types "yes", it
will print out "Thank you for using the store!" and exit the code. Otherwise, promptInput()
will be called.

Arena Logic
***********

The arena starts off by printing out all the boss stats and player stats. After this, it will 
print out "You will be fighting [name of boss]. Get ready!". It will then randomly decide whether
or not the boss will attack or defend, since those are the only two options the boss has.
It will then print out the main arena driver: "What would you like to do?: Attack Defend, Heal,
Stats".

If the player wants to attack, it will first determine if the enemy attacked or defended. If it
attacked, then it will subtract the player attack value from the enemy's health, and vice versa.
If the enemy defended, it will determine if the enemy's health - the difference in enemy's defend
value and player's attack value is greater than 0. If it is, then it will subtract the difference
in the enemy's defend value and the player's attack value from the enemy's health. Otherwise, it will
subtract 1 point from the enemy's overall health and print out "Your attack is very weak against
this enemy!". After this, it will check to see if the player has been defeated. If the player has
been defeated, it will print out "You have died" and exit the code of the arena. If not, then it will
determine if the enemy has been defeated. If it has been defeated, it will print out "Congratulations! 
the enemy has been defeated" and add 500 gold to the player's balance, as well as fully heal the player.
After this, the user will be prompted and asked: "Would you like to keep fighting or leave?"
If they type "leave", it will exit the code of the arena, and if they type "stay" it will iterate to the
next boss and redirect to the main arena driver. If the enemy has not been defeated, the player and
enemy heath will be printed out and it will return to the main arena driver.

If the player wants to defend, it will first determine if the enemy attacked or defended. If
the enemy defended, it will print out "Both characters defended!" and return to the main arena driver.
If the enemy attacked, it will check to see if the player's health - the difference in the player's
defend value and enemy attack value is greater than 0. If it is, it will subtract the difference of
the player's defend value and enemy attack value from the player's health. Otherwise, it will subtract
1 point from the player's overall health and print out "Your opponent barely hit you!". After this, it
will check to see if the player has been defeated. If the player has been defeated, it will print out
"You have died" and exit the code of the arena.

If the player wants to heal, it will first check to see if the player's bag is empty or if there is not
any healing items in the bag. If either of those are true, it will print out "You don't have any healing
items!" and return to the main arena driver. Otherwise, it will print out the contents of the bag and
print out "What would you like to use?". The user will input the item they want to use and it will check
to see if the input matches any item in the bag. If it does not, it will print out "Your input was invalid,
please try again". If it does, it will check to see if the inputted item is a healing item. If it is not,
it will print out "Your item is not a health item" and reprompt for input. If it is, then it will add the
health value of item to the player's health and pop the item out of the bag, print out "You have been
healed!" and return to the main arena driver.

If the player wants to view stats, it will simply print out the player and enemy stats and return to
the main arena driver.

Game Rules
**********

1) Start off by choosing to start a new game, or load a previous save file.
2) The user will then be redirected to a main game driver where they can choose to go to 
   the store, go to the arena, exit, equip an item, unequip an item, or view stats.
3) By accessing the store, the user can buy more weapons, food, and armor.
4) In the store, there are six possible options: add to cart, sell, view cart,
   checkout, remove item from cart, and leave store.
5) The store can be used as a means of upgrading your equipment to make fighting bosses
   at the arena easier.
6) By accessing the arena, the user can fight bosses and earn more gold. In the arena,
   there are four possible options: attack, defend, heal, and view stats.
7) The user can only leave the arena once either the player or a boss has been defeated.
8) The user can equip as many weapon or armor items as they want, in other words, their
   stats will stack. 
9) The goal is to beat all the bosses in the arena.


Game Instructions
*****************

- Starting a new game -
When the program initially runs, it will prompt the user if they want to load their previous
file, or start a new game. If you start a new game, regardless of if you have a previous save
or not, it will put everything in its default state. In other words, all salable items in the
store will be in the store's inventory, all bosses will be in the arena, and all of the player's
stats will be defaulted.

- Going to the store and buying/selling, etc. -
When the user wants to go to the store, it will follow the logic of the store (see above).

- Going to the arena, selecting an opponent, fighting etc. -
When the user wants to go to the arena, it will follow the logic of the arena (see above).

- Saving and loading a player's character -
When the user wants to load their previous save file, it will first check to see if the user has
a save file. If they do not, then they will receive a message saying "You don't have a save file!"
and reprompt for input. If they do, it will load it up automatically and go to the main game driver.

- How to view the character's stats / inventory -
To view the player's stats/inventory, simply type out the key letter associated with "view stats" in 
the main game driver or arena driver. Stats will also be printed out during the fighting process in the
arena.


Class Heirarchies
*****************

- Item Heirarchy -
There are three different types of items: weapons, health, and armor. Each of these item types has its
own class, and they are all derived from the BaseSalableItem class. Each of the weapon, health, and armor
classes has its own constructor that takes in the protected member variables of BaseSalableItem as parameters.
These include string name, string type, string description, int cost, and int stat.

- Enemy Heirarchy -
Enemy has several private member variables and public methods. Its private member variables include 
enemyHealth, enemyAttackValue, enemyBalance, enemyName, attackState, and defendState. Its methods consist
of getter methods for these states, a nondefault constructor, printEnemyStats(), enemyAttack(), and 
enemyDefend(). The Arena HAS-A Enemy, and both are represented by classes.

- Inventory Heirarchy - 
Inventory is the base class that the store is derived from. It contains one protected member variable, ItemList,
which is a vector that contains all of the items. Its methods include a constructor, printInventoryItemTokens(),
printItemList(), addSalableItem(), and checkInventorySize(). BaseSalableItem HAS-A inventory, and both
are represented by classes.

- Player / Character Heirarchy -
The Game class HAS-A player, and the player HAS-A BaseSalableItem. The player has several private
member variables, including a bag vector, an equippedItems vector, name, playerBalance, and health.
Its methods include a constructor, destructor, getter methods for bag and equippedItems, printBag(),
addtoplayerBalance(), addtoBag(), removefromBag(), and checkBagSize().


Input Data Files
****************
Armor.txt
Health.txt
Weapon.txt
Save.txt
Enemy.txt


