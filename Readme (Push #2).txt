Thomas Fowler
CST-210, MWF155A
Mark Reha
6 November 2016

FANTASY FIGHTING GAME PART I: THE STORE

Main Purpose
************
The main purpose of this whole project is to create a text based fantasy arena combat
game. The first part of this project involves creating a functional store that the user
can use to purchase better equipment (weapons, armor, and health). The user can then
use these items in the arena, which is the next part of the project. Each item will be
imported into the store's inventory from the weapon, armor, and health input files via
file/IO.

Game Description
****************
This game is based off of the Shaman hero from the popular online card game, Hearthstone.
The player will start off with a weapon, some armor and food to help him/her fight off
enemies in the arena. Enemies drop gold which can be spent in the store to upgrade equipment.
The items that are in the inventory of the store will be imported from their respected
input files.

Functionality
*************
This program will meet the main purpose in a variety of ways. The user will receive direct
feedback so he/she will always know what is going on. All user inputs will be text-based,
and it will allow the user to have full control of the fighter.

Approach + Implementation
*************************
The store will consist of six main actions, and each action will have its own key that is
inputted by the user:

 Add to cart (key: 0)
 Sell (key: 1)
 View cart (key: 2)
 Checkout (key: 3)
 Remove item from cart (key: 4)
 Leave store (key: 5)


At the start of the game, the player will have these default items:
 "Spirit Claws" weapon
 2 apples
 Weak armor

Game Rules
**********

1) Start off by fighting in the arena! Press the space bar to attack enemies.
2) Whenever you defeat an enemy, you will gain some gold that can be spent at the store.
2) Type in "Store" to access the store and buy more weapons, food, and armor.
3) In the store, there are six possible options: add to cart, sell, view cart,
   checkout, remove item from cart, and leave store.
4) The store can be used as a means of upgrading your equipment to make fighting enemies
   easier!
5) The goal is to get the best equipment possible so enemy fights will be effortless!

Design Features
***************
My vision is that the cart will be a vector that automatically expands and
shrinks as items are taken or removed from the cart. Doing so not only makes the most
sense, but it would also optimize speed and memory.

Throughout the scope of the code, there will be a universal "cancel" that will bring the
player to the main menu whenever they type the keyword "c". Additionally, I will implement
a method that will validate user input.

For now, I intend for there to be a total of 8 classes:
 Store
 StoreLogic
 BaseInventory
 Player
 BaseSalableItem
 Weapon
 Armor
 Health

Each class will have its own private / protected member variables and public methods. I will
exercise inheritance in this project by the "Has-A" and "Is-A" relationships between the
classes. For example, the classes Weapon, Armor, and Health all have an Is-A relationship
with the BaseSalableItem class.

List of Weapons
***************
 Spirit Claws (default weapon)
	- Cost: 100 gold
	- Description: "A beginner's choice!"
	- Attack Value: 30

 Hammer of Twilight
	- Cost: 750 gold
	- Description: "Packs a big punch!"
	- Attack Value: 70

 Doomhammer
	- Cost: 1000 gold
	- Description: "The best weapon in the game!"
	- Attack Value: 100

Challenges
**********
I think the hardest part for me will be translating my design into the syntax of the
code itself. Managing the vector(s) and optimizing speed and memory will be among one
of the many challenges in this project. Additionally, this project will require a lot
of time, and I plan on investing a lot more time in this project compared to past projects.

TODO
****

As of now, the only thing left to include is the code implementation. I will be using the
comments that I made in my code stubs to guide me through the process.

