Thomas Fowler
CST-210, MWF155A
Mark Reha
30 October 2016

FANTASY FIGHTING GAME PART I: THE STORE

Main Purpose
************
The main purpose of this whole project is to create a text based fantasy arena combat
game. The first part of this project involves creating a functional store that the user
can use to purchase better equipment (weapons, armor, and health). The user can then
use these items in the arena, which is the next part of the project.

Functionality
*************
This program will meet the main purpose in a variety of ways. The user will receive direct
feedback so he/she will always know what is going on. All user inputs will be text-based,
and it will allow the user to have full control of the fighter.

Approach + Implementation
*************************
I intend for the character / weapons to be based off of the Shaman hero from the popular 
online card game, Hearthstone. The store will consist of five main actions, and each 
action will have its own keyword that is inputted by the user:

- Add to cart (keyword: "add")
- Sell (keyword: "sell")
- View cart + inventory (keyword: "view")
- Checkout (keyword: "check")
- Leave store (keyword: "leave")

At the start of the game, the player will have these default items:
- "Spirit Claws" weapon
- 2 apples
- Leather armor

Design Features
***************
My vision is that the shopping cart will be a vector that automatically expands and
shrinks as items are taken or removed from the cart. Doing so not only makes the most
sense, but it would also optimize speed and memory.

Throughout the scope of the code, there will be a universal "cancel" that will bring the
player to the main menu whenever they type the keyword "c". Additionally, I will implement
a method that will validate user input.

For now, I intend for there to be a total of 7 classes:
- Store
- Inventory
- Player
- Salable_Item
- Weapon
- Armor
- Health

Each class will have its own private / protected member variables and public / protected
methods. I will exercise inheritance in this project by the "Has-A" and "Is-A"
relationships between the classes (see "Store UML.png" for visualization).

Challenges
**********
I think the hardest part for me will be translating my design into the code itself.
Managing the vector(s) and optimizing speed and memory will be among one of the many
challenges in this project. Additionally, this project will require a lot of time,
and I plan on investing a lot more time in this project compared to past projects.


